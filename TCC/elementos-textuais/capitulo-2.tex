\chapter{TXM e as Técnicas de Imageamento}\label{cap_TXM}

\section{Motivação Científica da Estação TXM}
Nas últimas décadas, o uso de técnicas analíticas baseadas em fontes de radiação síncrotron de alto brilho por cientistas do solo e do meio ambiente tornou-se comum\cite{livro_solo}. Dentre as análises utilizadas, destaca-se a espectromicroscopia de raios X, onde são utilizadas microssondas de raios X com grande resolução espacial para o estudo de espécies e elementos químicos utilizando-se de diferentes técnicas de espectroscopia.
 
Tendo isto em vista, nos últimos anos, a linha de luz SXS passou a utilizar uma nova estação experimental, denominada de TXM (Tender X-ray Microscope)\cite{CDR}, dedicada ao estudo de amostras biológicas, geológicas e ambientais. A TXM (a qual está sendo desenvolvida como um protótipo para a futura linha de luz do SIRIUS, a CARNAUBA\cite{carnauba}) é uma instrumentação dedicada a focalização de raios X na escala micrométrica para análises de $\mu$-XAS. Nesta estação, são utilizadas técnicas de varredura da amostra na região focal do sistema óptico da estação, enquanto a fluorescência da amostra é observada por um detector.

As amostras biológicas e geológicas analisadas na faixa de energia da SXS se destacam por sua heterogeneidade e composições químicas que podem variar grandemente dentro de suas estruturas\cite{dalton_presentation}. Logo, as técnicas de imageamento 2D e 3D têm se mostrado uma ferramenta muito importante para o estudo destas estruturas, por conter, de forma compacta tanto a informação da estrutura do espécime analisado como da composição elemental. Tendo este contexto em vista, a estação TXM foi projetada para possibilitar a implementação destas técnicas de varredura e imageamento.

Portanto a estação TXM tem o objetivo de estudar na faixa de energia da SXS ( 1000 to 5000 eV) a estrutura eletrônica e atômica de elementos de baixo número atômico (Z) por meio da espectroscopia de absorção de raios X, dando acessso a suas respectivas bordas K(Mg (Z=12, EK=1303 eV) to Ti (Z=22, EK=4966 eV)) performando técnicas relacionas a espectromicroscopia de raios X como o imageamento por fluorescência. Para tal, a TXM possui um conjunto de motores (para manipulação da amostra), de detectores e de um sistema óptico para focalização do microfeixe.

\subsection{Estação \textit{Tender X-Ray Microscope} (TXM)}

A TXM utiliza-se de um policapilar\footnote{Ao focalizar o feixe, o policapilar proporciona um ganho em intensidade de até 100 vezes, isto comparado com um \textit{pinhole} 0.02mm e um feixe incidente divergente de 0.6mm x 1.2mm} como elemento óptico para a focalização do feixe de raios X e assim aumentar a sensibilidade da incidência dos feixes. De maneira geral, o sistema opera focalizando o feixe de uma fonte de raios X de ordem de micrômetros em um ponto a uma distância focal de 3 mm. O uso dessa óptica de focalização policapilar aumenta significativamente a sensibilidade de detecção. O uso de um sistema monocapilar impõe algumas limitações, uma vez que não satisfaz as necessidades de razão de magnificação. Para atingir razões similares ao de sistema policapilar, seria necessário empregar uma distância focal elevada, o que resultaria em uma baixa eficiência devido a uma divergência da fonte. A Figura \ref{esquema_txm} apresenta a estação TXM, bem como um esquema da montagem do policapilar.


\begin{figure}[htb]
 \centering
  \label{estacao_TXM7}
 \caption{ (a)  Projeto da estação TXM para a técnica de Tomografia por fluorescência de raios X. Nesta imagem somente não está representado o conjunto da CCD que será usado para transmissão.   (b)   Estação TXM em Fevereiro de 2018, antes da instação dos estágios da Aerotech.}
  \begin{minipage}{0.4\textwidth}
    \centering
    \label{esquema_txm}
    \includegraphics[scale=0.35]{imagens/TXM_projeto.png}
    \legend{(a)}
  \end{minipage}
  \hfill
  \begin{minipage}{0.3\textwidth}
    \centering
     \label{txm_antes}
    \includegraphics[scale=0.27]{imagens/TXM_antamente.png}
    \legend{(b)}
  \end{minipage}
  \legend{Fonte: \citeonline{SXS-SHAREPOINT}}
\end{figure}


\begin{figure}[htb]
 \label{amostra_TXM}
 \centering
 \caption{ (a)  Exemplo da posição da amostra frente a janela de berílio e ao detector. Devido à distância focal do policapilar ser de 3$mm$, e a atenuação do sinal por parte do ar, a configuração experimental exige que o detector, amostra e janela de berílio estejam bem próximos.   (b)   Esquema do suporte do policapilar junto a janela de berílio por onde o feixe da linha sai.}
  \begin{minipage}{0.4\textwidth}
    \centering
    \label{esquema_amostra}
    \includegraphics[scale=0.4]{imagens/amostra_hexa_projeto.png}
    \legend{(a)}
  \end{minipage}
  \hfill
  \begin{minipage}{0.45\textwidth}
    \centering
     \label{esquema_poli}
    \includegraphics[scale=0.4]{imagens/policapilar.png}
    \legend{(b)}
  \end{minipage}
  \legend{Fonte: \citeonline{SXS-SHAREPOINT}}
\end{figure}

%%%%%%%%%%%%%%%

O conjunto de motores da TXM utilizados para manipulação da amostra é composto por um hexápode (modelo BORA, da Symétrie), um estágio de rotação com rolamento a ar (modelo ABRS-250MP, da Aerotech) e um estágio linear XY (modelo PlanarDL, da Aerotech), com as especificações presentes no Anexo A. Os equipamentos da Aerotech foram instalados no início do ano de 2018, sendo este o primeiro uso dos equipamentos na linha de Luz.
\begin{figure}[H]
	\caption{\label{motores_imagem}Motores à serem utilizados na estação TXM para imageamento 2D e 3D. }
	\begin{center}
	    \includegraphics[scale=0.31]{imagens/motores.jpg}
	\end{center}
	\legend{Fonte: Adaptado de (\citeonline{imagem_anel_png})}
\end{figure}
O sistema de detecção varia dependendo da técnica utilizada. Como neste trabalho o foco é direcionado para mapeamento e tomografia por fluorescência, o uso dos detectores de fluorescência compõe o sistema central de detecção. Para a estação TXM atualmente estão disponíveis 2 detectores de fluorescência para uso, o AMPTEK e o VORTEX.

Para detecção do fluxo de radiação incidente é utilizado um fotodiodo antes do sistema policapilar. Para a medida de transmissão na tomografia é utilizado um fotodiodo o qual tem a funcionalidade de medir o fluxo transmitido pela amostra. O sinal advindo do fotodiodo é detectado por um multímetro Keithley 6514/E.


\section{Imageamento 2D por fluorescência de raios X}

A figura \ref{scan_esquema} apresenta o esquema de um mapeamento 2D por fluorescência de raios X. É possível identificar o sinal de contagem de fótons de fluorescência provindos da amostra pelo espectro de fluorescência e definindo regiões de interesse (ROIs) podemos caracterizar a proporção de sinais provenientes da região excitada pelo feixe. Na figura podemos observar 3 sinais distintos de três 3 ROIs que representam 3 elementos diferentes (S,Si,P). No processo de reconstrução as áreas destas curvas são normalizadas e associadas a uma cor do espectro RGB. Portanto, o resultado final é um mapeamento do sinal de fluorescência originado por diferentes elementos.

O processo de formação da imagem desta técnica consiste na análise da aquisição dos dados de fluorescência em cada região da amostra. Portanto, a imagem derivada desta técnica indica três parâmetros pertinentes que estão totalmente relacionados à condições experimentais: o tamanho do pixel da imagem, a intensidade e/ou brilho e coloração. O tamanho do pixel está relacionado com o tamanho do passo do motor, e a intensidade está relacionada com excitação da amostra pelo feixe, ou seja, tempo de exposição e o perfil do espectro modulam o brilho observado. Por fim, as cores da imagem são geradas no padrão RGB, de modo que a composição das cores está relacionada à distribuição de três elementos, sendo cada elemento relacionando às cores vermelho, verde e azul respectivamente.

Atualmente essa técnica de mapeamento 2D por fluorescência já é empregada em algumas linhas de luz no UVX, como a XRF. Contudo há um interesse em aplicar esta configuração experimental no contexto da SXS devido à faixa de energia da SXS ser mais baixa, permitindo acessar espectros de materiais que são excitados na região de 1 a 5 keV como o Al, Si, P, S e Ca.

\begin{figure}[H]
	\caption{\label{scan_esquema}Imageamento 2D de uma folha realizada na SXS, sendo a energia do fóton do feixe incidente de 2500eV. }
	\begin{center}
	    \includegraphics[scale=0.25]{imagens/folha.png}
	\end{center}
	\legend{Fonte: Autoria Própria}
\end{figure}

\subsection{Configuração Experimental}
O objetivo da TXM é fazer análises em escalas micrométricas. Para tal, oposicionamento da amostra na TXM da SXS deve suprir isto tanto no posicionamento, no alinhamento, como durante a varredura do sistema da TXM. Vale ressaltar que a TXM, por  servir como protótipo para o sistema de posicionamento de microfoco da CARNAUBA ( linha de luz do SIRIUS) deve atender as especificações desta também. Logo, a resolução e estabilidade requeridas para o sistema estão diretamente relacionadas às demandas do projeto da CARNAUBA (dimensão do feixe no foco 30x30 $nm^2$) o qual é cerca de três ordens de magnitude maior que a necessária para o sistema de microfoco da SXS (dimensão do feixe no foco é de 20x20 $\mu m^2$)\cite{CDR}.

Como demonstrado na figura \ref{esquema_amostra}, em um experimento de imageamento 2D na  TXM a amostra é alinhada a um ângulo de 30$^\circ$ ou 45$^\circ$com o eixo do feixe incidente, com o detector fixado a à um ângulo de 90$^\circ$. O ângulo de 30$^\circ$  é escolhido em virtude da redução do ruído provindo do feixe espalhado (polarização horizontal do \textit{bending magnets}). Esta configuração de ângulo foi a mais aceitável, pois para ângulos maiores além da contagem de fótons detectados ser menor , a resolução também diminui visto que o feixe se deforma para uma elipse com uma maior excentricidade\footnote{isto implica que o feixe teria dimensões maiores em uma direção específica, alterando a resolução da imagem}. Por fim, depois do alinhamento a amostra deve ser a varrida de modo que esta permaneça no foco do policapilar e a distância com relação ao detector de fluorescência não varie.

Anteriormente, o processo de alinhamento do Hexápode trazia algumas limitações. Devido ao fato do Hexápode estar fixado a uma base sem grau de liberdade em ângulo (Figura \ref{txm_antes}), o seu centro de referência está rotacionado a um ângulo fixo de 30$^\circ$. Consequentemente, dependendo da espessura da amostra bidimensional era ainda necessário fazer um alinhamento extra nas direções X e Y (do Hexápode) para trazer o plano da amostra no foco do policapilar, o que dificultava o processo de alinhamento.


\section{Tomografia por fluorescência de raios X}
O conceito da técnica de tomografia por fluorescência de raios X é uma extensão da técnica de mapeamento 2D. O princípio é varrer a amostra em toda a sua superfície  com um feixe micrométrico, incluindo um movimento de rotação ao sistema. Após mapear a amostra em um plano fixo, esta é rotacionada a um ângulo $\theta$, realizando novamente o mapeamento na amostra, como demonstrado nas Figuras \ref{tomagem} e \ref{tomiagem}. Posteriormente estes dados são tratados e processados possibilitando a formação de imagens 3D da estrutura interna do espécime.

\begin{figure}[H]
	\caption{\label{tomagem}Representação da aquisição da fluorescência e transmissão emitida pela amostra durante a tomografia por fluorescência de raios X.}
	\begin{center}
	    \includegraphics[scale=0.25]{imagens/tomography.png}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline{tomagem}}
\end{figure}

\begin{figure}[H]
	\caption{\label{tomiagem}Representação da medida de tomografia utilizando a análise por fluorescência de raios X. Na TXM deve ser realizada uma varredura 2D na amostra antes de rotacionar para o próximo ponto, criando uma projeção a cada ângulo. }
	\begin{center}
	    \includegraphics[scale=0.1]{imagens/tomografia.png}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline{toma}}
\end{figure}

Diferente do mapeamento 2D, os dados coletados não se limitam somente aos dados da fluorescência mas também a informação da quantitade de feixe transmitidos(Figura \ref{tomagem}). Posteriormente estas informações são coletadas para formar os Sinogramas que depois são reconstruídos para gerarem a imagem 3D(Figura \ref{sino}).


Segundo \citeonline{maroto} o número de planos necessários para tomografia está diretamente relacionado com a a resolução requerida para a Reconstrução Tomográfica por Retroprojeção Filtrada (RTRF). Segundo a RTRF, o número de projeções necessárias segue aproximadamente esta relação $n_{planos}\approx n_{steps \_ horizontais}$\cite{maroto}, conforme apresentado na Figura \ref{tomei2}. Deste modo, para uma tomografia formada for projeções de imagens 2D de 30(altura)x60(largura) pixels,  o número mínimo de projeções é 60, logo o passo mínimo de $6^\circ$. Consequentemente, são necessárias 60 imagens 2D de dimensões para assim gerar informações suficientes para a reconstrução da tomografia.
\begin{figure}[htb]
	\caption{\label{sino}Representação da formação de um sinograma dada as medidas de projeção. Neste exemplo as projeções são unidimensionais da fluorescência e trasnmissão emitida pela amostra durante a tomografia por fluorescência de raios X.}
	\begin{center}
	    \includegraphics[scale=0.25]{imagens/sinograma.jpg}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline{loka}}
\end{figure}


Existem linhas de luz no UVX dedicadas a tomografia, como a IMX e a XRF. Contudo, a aplicação da técnica de tomografia por fluorescência somente é feita em uma  linha de luz no UVX, a XRF.

Apesar de na XRF o conceito da tomografia por fluorescência ser o mesmo  do planejado para a estação TXM, o propósito de se aplicar na linha SXS está relacionado com a faixa de energia. Uma vez que a energia da SXS é baixa, permitindo acessar espectros de materiais que são excitados na região de 1 a 5 keV como o Al, Si, P, S e Ca, diferentemente da XRF onde a faixa de energia é de 5keV a 10keV. Outro fator importante é que diferentemente das outras linhas de luz do UVX a SXS não possui cabana experimental, logo, para teste de instrumentação, a SXS possui uma maior liberdade, facilitando a instalação e teste de uma instrumentação nova. Sendo assim uma ótima oportunidade para a o grupo responsável pela linha CARNAUBA testar o conceito das técnicas da futura linha do Sirius ainda no UVX.

\begin{figure}[H]
	\caption{\label{tomei2}Representação da reconstrução de uma amostra de 128x128 pixels. As imagens mostram a relação entre o número de projeções necessárias para uma boa resolução na reconstrução.}
	\begin{center}
	    \includegraphics[scale=0.20]{imagens/projec.jpg}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline{maroto}}
\end{figure}

Por fim, vale ressaltar o impacto do tempo morto na viabilização da técnica de tomografia. Para formar uma única imagem de baixa resolução de 30x50 pixels, são necessárias em média 1h40 de aquisição\footnote{utilizando o sistema mais rápido de varredura que é pela Classe do Hexápode o qual já foi implementada ao Scan-Utils}, uma tomografia com 30 projeções de 30x50 pixels demorará por volta de 35 horas. Este resultado acabava impedindo o projeto e implementação da técnica na SXS dado a inviabilidade do tempo de medida, soma-se ainda a falta de um sistema de controle adequado e principalmente um setup experimental que proporcionasse o movimento de rotação.

 \subsection{Configuração Experimental}   
Para o caso da tomografia de raios X temos que girar a amostra em torno de um eixo de precisão mantendo-se a posição da amostra tão estável quanto possível, fazendo com que o eixo de precisão coincida com o feixe de raios X. O arranjo experimental proposto está mostrado na figura \ref{txm_antes} a. A montagem consiste em:
\subsubsection*{Motores}
\begin{itemize}
\item\textbf{Hexápode(BORA, SYMETRIE)}

Dedicado ao alinhamento e posicionamento da amostra;

Varredura no plano da amostra para formar as projeções 2D;

Será montado sobre um estágio de rotação do tipo “ air-bearing
 ABRS-250MP (Aerotech)”;

\item\textbf{Goniômetro air-bearing (ABRS-250MP (Aerotech))}
Provê a rotação com a precisão necessária para a tomografia;

\item\textbf{Estágios Aerotech (PlanarDL 300XY (Aerotech))}

O conjunto todo será montado sobre uma mesa planar com estágios de movimentação nas direções XZ (PlanarDL 300XY (Aerotech)), que tem por função localizar o feixe de RX sobre o eixo de rotação dentro do tamanho do feixe e da profundidade focal. Além de fazer os ajustes necessários para o alinhamento.
 \end{itemize}

\subsubsection*{Detectores}
\begin{itemize}
\item\textbf{Amptek(X123, Amptek)}
\begin{figure}[H]
	\caption{\label{tomei}Detector Amptek X123}
	\begin{center}
	    \includegraphics[scale=0.26]{imagens/amptek.jpg}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline{amptek}}
\end{figure}
\item\textbf{Vortex(ME4-Asic, HITACHI)}

Comparado ao detector Amptek (25$mm^{2}$ de área total) este detector possui um ângulo sólido maior por ter área total de 200$mm^{2}$.

\begin{figure}[H]
	\caption{\label{tomei}Detector Vortex ME4-Asic}
	\begin{center}
	    \includegraphics[scale=0.16]{imagens/vortex.jpg}
	\end{center}
	\legend{Fonte: Adaptado de \citeonline{vortex_site}}
\end{figure}
 \end{itemize}
\section{Configuração do Modo de Aquisição e Movimento para Imageamento e Tomografia} 

O sistema adotado para a aquisição é a varredura \textit{step by step}. Nele, os motores se movimentam até a posição alvo e após alcançarem esta posição disparam um sinal (\textit{trigger}) para que o detector inicie a aquisição. Este sinal pode ser tanto via software, como por um sinal elétrico. No caso da SXS, o \textit{trigger} é via software utilizando o sistema EPICS. Deste modo, um comando é enviado para o detector iniciar a aquisição quando os motores alcançarem a posição alvo. 

\begin{figure}[htb]
	\caption{\label{varredura_esquema}Representação do sistema lógico de uma varredura \textit{step by step}. a)Sincronização ideal entre a movimentação e aquisição. b)Sincronização real entre os sistemas representando os possíveis pontos de latência.}
	\begin{center}
	    \includegraphics[scale=0.3]{imagens/real_ideal.png}
	\end{center}
	\legend{Fonte: Autoria Própria}
\end{figure} 
 
Após o \textit{trigger} ser acionado, o detector inicia a aquisição durante o período estipulado pelo usuário. A Figura \ref{varredura_esquema} apresenta a configuração ideal para varredura \textit{step by step}. Ao final da aquisição os motores se movem para a próxima posição alvo, iniciando novamente a aquisição. 

Contudo, o sistema atual de varredura 2D apresenta uma latência que ocasiona um tempo morto alto de origem, à priori, desconhecida. Como representado na Figura \ref{varredura_esquema}, esta latência pode ter origem em diversos pontos durante a varredura, tanto relacionados com o controle da aquisição, da movimentação ou da integração destes dois sistemas. Por exemplo, o controle de movimentação pode estar adicionando um tempo extra ao sistema por não possuir um bom desempenho ao monitorar a posição do motor, acionando o \textit{trigger} posteriormente. Para o sistema de aquisição, tempo morto pode estar relacionado com o tempo de resposta do detector. Por fim, a origem da latência pode vir da integração dos dois sistemas, onde o controle que aciona aquisição e movimentação pode não ter o desempenho esperado por ter problemas na comunicação com os equipamentos. 


 

 
