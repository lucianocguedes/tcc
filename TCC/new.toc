\changetocdepth {4}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdução}{17}{chapter*.16}
\contentsline {part}{\partnumberline {I}\MakeTextUppercase {Preparação da pesquisa}}{19}{part.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {CNPEM}}{20}{chapter.1}
\contentsline {section}{\numberline {1.1}Fonte de Luz síncrotron}{20}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Linha SXS(Soft X-Ray Spectroscopy)}{22}{subsection.1.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {TXM e as Técnicas de Imageamento}}{50}{chapter.6}
\contentsline {section}{\numberline {6.1}Motivação Científica da Estação TXM}{50}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Estação Tender X-Ray Microscope (TXM)}{50}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}Imageamento 2D por fluorescência de raios-X}{53}{section.6.2}
\contentsline {subsubsection}{\numberline {6.2.0.1}Configuração Experimental}{53}{subsubsection.6.2.0.1}
\contentsline {section}{\numberline {6.3}Tomografia por fluorescência de raios-X}{54}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Setup Físico}{55}{subsection.6.3.1}
\contentsline {section}{\numberline {6.4}Configuração do Sistema de Aquisição e Movimento para Imageamento e Tomografia}{56}{section.6.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Sistemas de controle dos experimentos na TXM}}{34}{chapter.3}
\contentsline {section}{\numberline {3.1}Sistemas de controle no LNLS}{34}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}EPICS}{34}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Exemplo conceitual de Input/Output Controller(IOC)}{35}{subsubsection.3.1.1.1}
\contentsline {subsection}{\numberline {3.1.2}Python}{37}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Scripts de varredura na TXM}{37}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Vambora.sh}{39}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}HexapodeClass.py }{40}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Comparando a estrutura do Vambora.sh e HexapodeClass.py}{40}{subsection.3.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Metodologia}}{42}{chapter.4}
\contentsline {section}{\numberline {4.1}Imageamento 2D por florescência}{43}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Configurações do Aparato Experimental}{43}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Movimentação e Controle}{43}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Aquisição}{43}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Integração com a Aquisição}{44}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2} Implementação e testes da Tomografia}{44}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Configurações do Aparato Experimental}{44}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Integração}{44}{subsection.4.2.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Resultados}}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Instalação e integração dos dispositivos a linha de Luz SXS}{45}{section.5.1}
\contentsline {section}{\numberline {5.2}Imageamento 2D por florescência}{45}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Configurações do Aparato Experimental}{45}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Movimentação e Controle}{46}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}A) Pontos Críticos dos modos de movimento do Hexápode}{46}{subsubsection.5.2.2.1}
\contentsline {paragraph}{\numberline {5.2.2.1.1}Movimento Único}{46}{paragraph.5.2.2.1.1}
\contentsline {part}{\partnumberline {II}\MakeTextUppercase {Dicas de úteis}}{49}{part.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {TXM e as Técnicas de Imageamento}}{50}{chapter.6}
\contentsline {section}{\numberline {6.1}Motivação Científica da Estação TXM}{50}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Estação Tender X-Ray Microscope (TXM)}{50}{subsection.6.1.1}
\contentsline {section}{\numberline {6.2}Imageamento 2D por fluorescência de raios-X}{53}{section.6.2}
\contentsline {subsubsection}{\numberline {6.2.0.1}Configuração Experimental}{53}{subsubsection.6.2.0.1}
\contentsline {section}{\numberline {6.3}Tomografia por fluorescência de raios-X}{54}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Setup Físico}{55}{subsection.6.3.1}
\contentsline {section}{\numberline {6.4}Configuração do Sistema de Aquisição e Movimento para Imageamento e Tomografia}{56}{section.6.4}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Conclusão}{59}{chapter*.29}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{60}{section*.32}
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Ap\^endices}}{61}{section*.33}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Quisque libero justo}}{62}{appendix.A}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}\MakeTextUppercase {Nullam elementum urna vel imperdiet sodales elit ipsum pharetra ligula ac pretium ante justo a nulla curabitur tristique arcu eu metus}}{63}{appendix.B}
\cftinsert {AAA}
\renewcommand *{\cftappendixname }{ANEXO \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Anexos}}{64}{section*.34}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Morbi ultrices rutrum lorem.}}{65}{appendix.anexochapback.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}\MakeTextUppercase {Cras non urna sed feugiat cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus}}{66}{appendix.anexochapback.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {C}\MakeTextUppercase {Fusce facilisis lacinia dui}}{67}{appendix.anexochapback.3}
\vspace {\cftbeforechapterskip }
