\changetocdepth {4}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdução}{16}{chapter*.16}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {CNPEM}}{18}{chapter.1}
\contentsline {section}{\numberline {1.1}Fonte de Luz síncrotron}{18}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Linha SXS(Soft X-Ray Spectroscopy)}{20}{subsection.1.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {TXM e as Técnicas de Imageamento}}{23}{chapter.2}
\contentsline {section}{\numberline {2.1}Motivação Científica da Estação TXM}{23}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Estação Tender X-Ray Microscope (TXM)}{23}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Imageamento 2D por fluorescência de raios-X}{26}{section.2.2}
\contentsline {subsubsection}{\numberline {2.2.0.1}Configuração Experimental}{26}{subsubsection.2.2.0.1}
\contentsline {section}{\numberline {2.3}Tomografia por fluorescência de raios-X}{27}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Setup Físico}{28}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Configuração do Sistema de Aquisição e Movimento para Imageamento e Tomografia}{29}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Sistemas de controle dos experimentos na TXM}}{32}{chapter.3}
\contentsline {section}{\numberline {3.1}Sistemas de controle no LNLS}{32}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}EPICS}{32}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Exemplo conceitual de Input/Output Controller(IOC)}{33}{subsubsection.3.1.1.1}
\contentsline {subsection}{\numberline {3.1.2}Python}{35}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Scripts de varredura na TXM}{35}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Vambora.sh}{37}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}HexapodeClass.py }{38}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Comparando a estrutura do Vambora.sh e HexapodeClass.py}{38}{subsection.3.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Metodologia}}{40}{chapter.4}
\contentsline {section}{\numberline {4.1}Imageamento 2D por florescência}{41}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Configurações do Aparato Experimental}{41}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Movimentação e Controle}{41}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Aquisição}{41}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Integração com a Aquisição}{42}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2} Implementação e testes da Tomografia}{42}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Configurações do Aparato Experimental}{42}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Integração}{42}{subsection.4.2.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Resultados}}{43}{chapter.5}
\contentsline {section}{\numberline {5.1}Instalação e integração dos dispositivos a linha de Luz SXS}{43}{section.5.1}
\contentsline {section}{\numberline {5.2}Imageamento 2D por florescência}{43}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Configurações do Aparato Experimental}{43}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Movimentação e Controle}{44}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}A) Pontos Críticos dos modos de movimento do Hexápode}{44}{subsubsection.5.2.2.1}
\contentsline {paragraph}{\numberline {5.2.2.1.1}Movimento Único}{44}{paragraph.5.2.2.1.1}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Conclusão}{47}{chapter*.24}
\vspace {\cftbeforechapterskip }
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Ap\^endices}}{48}{section*.26}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Quisque libero justo}}{49}{appendix.A}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}\MakeTextUppercase {Nullam elementum urna vel imperdiet sodales elit ipsum pharetra ligula ac pretium ante justo a nulla curabitur tristique arcu eu metus}}{50}{appendix.B}
\cftinsert {AAA}
\renewcommand *{\cftappendixname }{ANEXO \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Anexos}}{51}{section*.27}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Morbi ultrices rutrum lorem.}}{52}{appendix.anexochapback.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}\MakeTextUppercase {Cras non urna sed feugiat cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus}}{53}{appendix.anexochapback.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {C}\MakeTextUppercase {Fusce facilisis lacinia dui}}{54}{appendix.anexochapback.3}
\vspace {\cftbeforechapterskip }
