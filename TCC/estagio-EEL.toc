\changetocdepth {4}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdução}{3}{chapter*.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {CNPEM}}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Fonte de Luz síncrotron}{5}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Linha SXS(Soft X-Ray Spectroscopy)}{7}{subsection.1.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {TXM e as Técnicas de Imageamento}}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}Motivação Científica da Estação TXM}{10}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Estação Tender X-Ray Microscope (TXM)}{11}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Imageamento 2D por fluorescência de raios-X}{13}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Configuração Experimental}{15}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Tomografia por fluorescência de raios-X}{15}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Configuração Experimental}{18}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Configuração do Modo de Aquisição e Movimento para Imageamento e Tomografia}{20}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {DESCRIÇÃO DAS ATIVIDADES REALIZADAS }}{22}{chapter.3}
\contentsline {section}{\numberline {3.1}Objetivo do Estágio}{22}{section.3.1}
\contentsline {section}{\numberline {3.2}Plano de Atividades}{22}{section.3.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Aprendizagem Obtida}}{25}{chapter.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Perspectivas Futuras}}{26}{chapter.5}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{CONCLUSÃO}{29}{chapter*.13}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{30}{section*.16}
\vspace {\cftbeforechapterskip }
