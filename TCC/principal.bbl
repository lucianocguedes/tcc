\providecommand{\abntreprintinfo}[1]{%
 \citeonline{#1}}
\setlength{\labelsep}{0pt}\begin{thebibliography}{}
\providecommand{\abntrefinfo}[3]{}
\providecommand{\abntbstabout}[1]{}
\abntbstabout{v-1.9.6 }

\bibitem[Abbate et al. 1999]{artigo_SXS}
\abntrefinfo{Abbate et al.}{ABBATE et al.}{1999}
{ABBATE, M. et al. The soft x-ray spectroscopy beamline at the lnls: technical
  description and commissioning results.
\emph{Journal of Synchrotron Radiation}, Wiley Online Library, v.~6, n.~5, p.
  964--972, 1999.}

\bibitem[Abdala 2018]{dalton_presentation}
\abntrefinfo{Abdala}{ABDALA}{2018}
{ABDALA, D.~B. \emph{Applications of Synchrotron-based Techniques in
  Agri-Environmental Sciences}. 2018. 5th School on X-Ray Spectroscopy Methods.
Dispon{\'\i}vel em:
  \url{http://pages.cnpem.br/schoolofxas/wp-content/uploads/sites/54/2016/09/XRF\_microscopymicro-spectroscopy\_module\_DAbdala.pdf}.
  Acesso em: 26 out. 2018.}

\bibitem[AMPTEK 2018]{amptek}
\abntrefinfo{AMPTEK}{AMPTEK}{2018}
{AMPTEK. \emph{X-123 Complete X-Ray Spectrometer with Si-PIN Detector}. 2018.
  AMPTEK, Inc.
Dispon{\'\i}vel em:
  \url{http://amptek.com/products/x-123-complete-x-ray-spectrometer-with-si-pin-detector/\#7}.
  Acesso em: 29 out. 2018.}

\bibitem[BUENO e CALIARI 2018]{metrologia}
\abntrefinfo{BUENO e CALIARI}{BUENO; CALIARI}{2018}
{BUENO, C.; CALIARI, R. \emph{Caracterização DL300 XY e ABRS 250 MP}. 2018.
  Relatório Técnico.}

\bibitem[Cardoso, Resende e Marques 2011]{LINAC}
\abntrefinfo{Cardoso, Resende e Marques}{CARDOSO; RESENDE; MARQUES}{2011}
{CARDOSO, F.; RESENDE, X.; MARQUES, S. Commissioning of a bpm system for the
  lnls booster to storage ring transfer line. In:  \emph{Conf. Proc.} [S.l.:
  s.n.], 2011. v. 110328, n. PAC-2011-MOP166, p. 405--407.}

\bibitem[Cherry et al. 2004]{loka}
\abntrefinfo{Cherry et al.}{CHERRY et al.}{2004}
{CHERRY, S.~R. et al. Physics in nuclear medicine.
\emph{Medical Physics}, Wiley Online Library, v.~31, n.~8, p. 2370--2371,
  2004.}

\bibitem[CNPEM 2018]{CNPEM_site_home}
\abntrefinfo{CNPEM}{CNPEM}{2018}
{CNPEM. \emph{Centro Nacional De Pesquisa em Energia e Materiais}. 2018. Home
  Page.
Dispon{\'\i}vel em: \url{http://cnpem.br}. Acesso em: 19 out. 2018.}

\bibitem[CNPEM-SXS 2018]{linha-sxs_cnpem_site}
\abntrefinfo{CNPEM-SXS}{CNPEM-SXS}{2018}
{CNPEM-SXS. \emph{Visão Geral}. 2018. SXS.
Dispon{\'\i}vel em:
  \url{https://www.lnls.cnpem.br/linhas-de-luz/sxs/overview/}. Acesso em: 22
  out. 2018.}

\bibitem[EPICS-ANL 2018]{epics_site}
\abntrefinfo{EPICS-ANL}{EPICS-ANL}{2018a}
{EPICS-ANL. \emph{Experimental Physics and Industrial Control System}. 2018.
  Argonne National Laboratory.
Dispon{\'\i}vel em: \url{https://epics.anl.gov/}. Acesso em: 10 out. 2018.}

\bibitem[EPICS-ANL 2018]{motor_record}
\abntrefinfo{EPICS-ANL}{EPICS-ANL}{2018b}
{EPICS-ANL. \emph{Motor Record and related software}. 2018. Argonne National
  Laboratory.
Dispon{\'\i}vel em:
  \url{https://epics.anl.gov/bcda/synApps/motor/R6-10/motorRecord.html}. Acesso
  em: 15 out. 2018.}

\bibitem[ESTADÃO 2018]{imagem_anel_png}
\abntrefinfo{ESTADÃO}{ESTADÃO}{2018}
{ESTADÃO. \emph{O que é luz síncrotron?} 2018. .EDU.
Dispon{\'\i}vel em:
  \url{http://4.bp.blogspot.com/\_DMKBYG4chXA/SxK4TEwXRJI/AAAAAAAAALo/bfg8MUmGvtE/s1600/luz+sincrotron.jpg}.
  Acesso em: 22 out. 2018.}

\bibitem[Fedel et al. 2018]{fedel2018python}
\abntrefinfo{Fedel et al.}{FEDEL et al.}{2018}
{FEDEL, G. et al. Python for user interfaces at sirius.
2018.}

\bibitem[Fessler 2009]{maroto}
\abntrefinfo{Fessler}{FESSLER}{2009}
{FESSLER, J. Analytical tomographic image reconstruction methods.
\emph{Image Reconstruction: Algorithms and Analysis}, v.~66, p.~67, 2009.}

\bibitem[Garc{\'\i}a 2016]{SINCRTRON-RADIATION}
\abntrefinfo{Garc{\'\i}a}{GARC{\'I}A}{2016}
{GARC{\'I}A, G. \emph{Synchrotron radiation: basics, methods and applications}.
  [S.l.]: Taylor \& Francis, 2016.}

\bibitem[HITACHI 2018]{vortex_site}
\abntrefinfo{HITACHI}{HITACHI}{2018}
{HITACHI. \emph{Vortex®-ME4 X-ray Detector.} 2018. Hitachi High-Technologies
  Science America.
Dispon{\'\i}vel em:
  \url{https://www.hitachi-hightech.com/hhs-us/product\_detail/?pn=ana-vortex-me4}.
  Acesso em: 29 out. 2018.}

\bibitem[LNLS 2018]{LNLS_site_home}
\abntrefinfo{LNLS}{LNLS}{2018}
{LNLS. \emph{Laboratório Nacional de Luz Síncrotron}. 2018. O LNLS.
Dispon{\'\i}vel em: \url{https://www.lnls.cnpem.br/o-lnls/}. Acesso em: 19 out.
  2018.}

\bibitem[Mesquita 2011]{bordak}
\abntrefinfo{Mesquita}{MESQUITA}{2011}
{MESQUITA, A.
\emph{S{\'\i}ntese e caracteriza{\c{c}}{\~a}o estrutural e diel{\'e}trica de
  compostos ferroel{\'e}tricos Pb1-xRxZr0, 40Ti0, 60O3 (R= Ba, La)}.
Tese (Doutorado) --- Universit{\'e} Paris-Est, 2011.}

\bibitem[NEWVILLE 2014]{pyepicsapres}
\abntrefinfo{NEWVILLE}{NEWVILLE}{2014}
{NEWVILLE, M. \emph{PyEpics, Epics Channel Access for Python}. 2014.}

\bibitem[NEWVILLE 2016]{pyepicsapres_site}
\abntrefinfo{NEWVILLE}{NEWVILLE}{2016}
{NEWVILLE, M. \emph{PyEpics: Python Epics Channel Access}. 2016. Center for
  Advanced Radiation Sources.V 3.2.5.
Dispon{\'\i}vel em:
  \url{http://cars9.uchicago.edu/software/python/pyepics3/pyepics.pdf}. Acesso
  em: 10 out. 2018.}

\bibitem[NEWVILLE 2018]{vortex_git}
\abntrefinfo{NEWVILLE}{NEWVILLE}{2018}
{NEWVILLE, M. \emph{An EPICS driver based on areaDetector for Quantum Detector
  Xspress3 electronics.} 2018. GitHub.
Dispon{\'\i}vel em: \url{https://github.com/epics-modules/xspress3}. Acesso em:
  17 out. 2018.}

\bibitem[O'Sullivan et al. 2017]{toma}
\abntrefinfo{O'Sullivan et al.}{O'SULLIVAN et al.}{2017}
{O'SULLIVAN, J. D.~B. et al. X-ray micro-computed tomography (μct): an
  emerging opportunity in parasite imaging.
\emph{Parasitology}, Cambridge University Press, v.~145, n.~7, p. 848–854,
  2017.}

\bibitem[PEREIRA e Lopes 2011]{tomagem}
\abntrefinfo{PEREIRA e Lopes}{PEREIRA; LOPES}{2011}
{PEREIRA, G.~R.; LOPES, R.~T. X-ray fluorescence microtomography in biological
  applications. In:  \emph{Computed Tomography-Special Applications}. [S.l.]:
  InTech, 2011.}

\bibitem[Perez et al. 2018]{carnauba}
\abntrefinfo{Perez et al.}{PEREZ et al.}{2018}
{PEREZ, C.~A. et al. The coherent x-ray sub-micro and nanoprobe stations for
  the carnauba beamline at the sirius-lnls storage ring.
\emph{Microscopy and Microanalysis}, Cambridge University Press, v.~24, n.~S2,
  p. 330--331, 2018.}

\bibitem[Peth et al. 2008]{livro_solo}
\abntrefinfo{Peth et al.}{PETH et al.}{2008}
{PETH, S. et al. Three-dimensional quantification of intra-aggregate pore-space
  features using synchrotron-radiation-based microtomography.
\emph{Soil Science Society of America Journal}, Soil Science Society, v.~72,
  n.~4, p. 897--907, 2008.}

\bibitem[Piton et al. 2012]{scaler}
\abntrefinfo{Piton et al.}{PITON et al.}{2012}
{PITON, J.~R. et al. Hyppie: A hypervisored pxi for physics instrumentation
  under epics.
\emph{MOPG031, Proc. BIW12}, 2012.}

\bibitem[RIVERS 2017]{amptek_mca}
\abntrefinfo{RIVERS}{RIVERS}{2017}
{RIVERS, M. \emph{EPICS MCA Amptek Driver}. 2017. Center for Advanced Radiation
  Sources.V 3.2.5.
Dispon{\'\i}vel em:
  \url{http://cars9.uchicago.edu/software/epics/mcaAmptek.html}. Acesso em: 20
  out. 2018.}

\bibitem[RIVERS 2018]{amptek_mca_git}
\abntrefinfo{RIVERS}{RIVERS}{2018}
{RIVERS, M. \emph{EPICS support for multi-channel analyzers (MCA) and
  multi-channel scalers (MCS)}. 2018. GitHub.
Dispon{\'\i}vel em: \url{https://github.com/epics-modules/mca}. Acesso em: 20
  out. 2018.}

\bibitem[Slepicka et al. 2015]{Py4Syn}
\abntrefinfo{Slepicka et al.}{SLEPICKA et al.}{2015}
{SLEPICKA, H.~H. et al. Py4syn: Python for synchrotrons.
\emph{Journal of Synchrotron Radiation}, v.~22, n.~5, p. 1182--1189, 2015.
ISSN 1600-5775.
Dispon{\'\i}vel em:
  \url{http://scripts.iucr.org/cgi-bin/paper?S1600577515013715}.}

\bibitem[VICENTIN 2018]{SXS-SHAREPOINT}
\abntrefinfo{VICENTIN}{VICENTIN}{2018}
{VICENTIN, F.~C. \emph{Beamline ​​overview}. 2018. Sharepoint Homepage.
Dispon{\'\i}vel em:
  \url{https://www.lnls.cnpem.br/linhas-de-luz/sxs/overview/}. Acesso em: 22
  out. 2018.}

\bibitem[VICENTIN 2016]{CDR}
\abntrefinfo{VICENTIN}{VICENTIN}{2016}
{VICENTIN, F. e.~a. \emph{Tender X-ray Microscope:prototype to be installed at
  the SXS beamline}. 2016. Conceptual Design Report.}

\end{thebibliography}
