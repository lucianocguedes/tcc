\changetocdepth {4}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdução}{17}{chapter*.16}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {CNPEM}}{19}{chapter.1}
\contentsline {section}{\numberline {1.1}Fonte de Luz síncrotron}{19}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Linha SXS(Soft X-Ray Spectroscopy)}{21}{subsection.1.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {TXM e as Técnicas de Imageamento}}{24}{chapter.2}
\contentsline {section}{\numberline {2.1}Motivação Científica da Estação TXM}{24}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Estação Tender X-Ray Microscope (TXM)}{24}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Imageamento 2D por fluorescência de raios-X}{27}{section.2.2}
\contentsline {subsubsection}{\numberline {2.2.0.1}Configuração Experimental}{27}{subsubsection.2.2.0.1}
\contentsline {section}{\numberline {2.3}Tomografia por fluorescência de raios-X}{28}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Setup Físico}{29}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Configuração do Sistema de Aquisição e Movimento para Imageamento e Tomografia}{30}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Sistemas de controle dos experimentos na TXM}}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}Sistemas de controle no LNLS}{33}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}EPICS}{33}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Exemplo conceitual de Input/Output Controller(IOC)}{34}{subsubsection.3.1.1.1}
\contentsline {subsection}{\numberline {3.1.2}Python}{36}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Scripts de varredura na TXM}{36}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Vambora.sh}{38}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}HexapodeClass.py }{39}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Comparando a estrutura do Vambora.sh e HexapodeClass.py}{39}{subsection.3.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Metodologia}}{41}{chapter.4}
\contentsline {section}{\numberline {4.1}Imageamento 2D por florescência}{42}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Configurações do Aparato Experimental}{42}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Movimentação e Controle}{42}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Aquisição}{42}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Integração com a Aquisição}{43}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2} Implementação e testes da Tomografia}{43}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Configurações do Aparato Experimental}{43}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Integração}{43}{subsection.4.2.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Resultados}}{44}{chapter.5}
\contentsline {section}{\numberline {5.1}Instalação e integração dos dispositivos a linha de Luz SXS}{44}{section.5.1}
\contentsline {section}{\numberline {5.2}Imageamento 2D por florescência}{44}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Configurações do Aparato Experimental}{44}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Movimentação e Controle}{45}{subsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.2.1}A) Pontos Críticos dos modos de movimento do Hexápode}{45}{subsubsection.5.2.2.1}
\contentsline {paragraph}{\numberline {5.2.2.1.1}Movimento Único}{45}{paragraph.5.2.2.1.1}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Conclusão}{47}{chapter*.24}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{48}{section*.27}
\renewcommand *{\cftappendixname }{AP\^ENDICE \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Ap\^endices}}{50}{section*.28}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Quisque libero justo}}{51}{appendix.A}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}\MakeTextUppercase {Nullam elementum urna vel imperdiet sodales elit ipsum pharetra ligula ac pretium ante justo a nulla curabitur tristique arcu eu metus}}{52}{appendix.B}
\cftinsert {AAA}
\renewcommand *{\cftappendixname }{ANEXO \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Anexos}}{53}{section*.29}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Morbi ultrices rutrum lorem.}}{54}{appendix.anexochapback.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {B}\MakeTextUppercase {Cras non urna sed feugiat cum sociis natoque penatibus et magnis dis parturient montes nascetur ridiculus mus}}{55}{appendix.anexochapback.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {C}\MakeTextUppercase {Fusce facilisis lacinia dui}}{56}{appendix.anexochapback.3}
\vspace {\cftbeforechapterskip }
