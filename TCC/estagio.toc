\changetocdepth {4}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdução}{7}{chapter*.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {CNPEM}}{9}{chapter.1}
\contentsline {section}{\numberline {1.1}Fonte de Luz síncrotron}{9}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Linha SXS (\textit {Soft X-Ray Spectroscopy})}{11}{subsection.1.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {TXM e as Técnicas de Imageamento}}{14}{chapter.2}
\contentsline {section}{\numberline {2.1}Motivação Científica da Estação TXM}{14}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Estação \textit {Tender X-Ray Microscope} (TXM)}{14}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Imageamento 2D por fluorescência de raios X}{17}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Configuração Experimental}{19}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Tomografia por fluorescência de raios X}{19}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Configuração Experimental}{22}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Configuração do Modo de Aquisição e Movimento para Imageamento e Tomografia}{24}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Sistemas de controle dos experimentos na estação experimental TXM}}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Sistemas de controle no LNLS}{25}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}EPICS}{25}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Exemplo conceitual de Input/Output Controller(IOC)}{26}{subsubsection.3.1.1.1}
\contentsline {subsection}{\numberline {3.1.2}Python}{28}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Sistema de controle da varredura na TXM}{29}{section.3.2}
\contentsline {section}{\numberline {3.3}Scripts de varredura na TXM}{30}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}\textit {Vambora.sh}}{31}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}\textit {HexapodeClass.py}}{33}{subsection.3.3.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Metodologia}}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Imageamento 2D por florescência}{36}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Configurações do Aparato Experimental}{36}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Movimentação e Controle}{36}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Aquisição}{36}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Integração com a Aquisição}{37}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2} Implementação e testes da Tomografia}{37}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Configurações do Aparato Experimental}{37}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Integração}{37}{subsection.4.2.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Resultados}}{38}{chapter.5}
\contentsline {section}{\numberline {5.1}Instalação e integração dos dispositivos na linha de Luz SXS}{38}{section.5.1}
\contentsline {section}{\numberline {5.2}Configurações do Aparato Experimental}{38}{section.5.2}
\contentsline {section}{\numberline {5.3}Aquisição}{39}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Controle da Aquisição}{39}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Visualização e Interface Gráfica}{41}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Movimentação e Controle}{42}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Análise dos modos de movimento do Hexápode}{42}{subsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.1.1}Movimento Único}{42}{subsubsection.5.4.1.1}
\contentsline {subsubsection}{\numberline {5.4.1.2}Movimento Sequencial}{45}{subsubsection.5.4.1.2}
\contentsline {subsection}{\numberline {5.4.2}Comparando a estrutura do \textit {Vambora.sh} e \textit {HexapodeClass.py}}{48}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Propostas para a movimentação nos eixos}{50}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Scripts e Modos de Movimento Propostos}{51}{subsection.5.4.4}
\contentsline {subsubsection}{\numberline {5.4.4.1}Proposta 1:Classe$\_$Hexapode.py}{51}{subsubsection.5.4.4.1}
\contentsline {subsubsection}{\numberline {5.4.4.2}Proposta 2: rumo$\_$ao$\_$hexa.py}{52}{subsubsection.5.4.4.2}
\contentsline {subsection}{\numberline {5.4.5}Comparação do desempenho em Tempo Morto dos Scripts Anteriores com os Scripts Propostos}{54}{subsection.5.4.5}
\contentsline {section}{\numberline {5.5}Resultados Preliminares de Imageamento}{55}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Resultados Preliminares de Aquisições realizadas com as soluções propostas para o imageamento 2D}{55}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Resultados Preliminares de Tomografia por Fluorescência}{57}{subsection.5.5.2}
\contentsline {section}{\numberline {5.6}Integração e Interface de Controle}{62}{section.5.6}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Perspectivas Futuras}}{66}{chapter.6}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{CONCLUSÃO}{69}{chapter*.19}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{71}{section*.22}
\cftinsert {AAA}
\renewcommand *{\cftappendixname }{ANEXO \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Anexos}}{74}{section*.23}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Informações sobre os motores}}{76}{appendix.anexochapback.1}
