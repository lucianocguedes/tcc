\changetocdepth {4}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdução}{15}{chapter*.10}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {CNPEM}}{17}{chapter.1}
\contentsline {section}{\numberline {1.1}Fonte de Luz síncrotron}{17}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Linha SXS (\textit {Soft X-Ray Spectroscopy})}{19}{subsection.1.1.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {TXM e as Técnicas de Imageamento}}{22}{chapter.2}
\contentsline {section}{\numberline {2.1}Motivação Científica da Estação TXM}{22}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Estação \textit {Tender X-Ray Microscope} (TXM)}{23}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Imageamento 2D por fluorescência de raios X}{25}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Configuração Experimental}{27}{subsection.2.2.1}
\contentsline {section}{\numberline {2.3}Tomografia por fluorescência de raios X}{27}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Configuração Experimental}{30}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Configuração do Modo de Aquisição e Movimento para Imageamento e Tomografia}{32}{section.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Sistemas de controle dos experimentos na estação experimental TXM}}{33}{chapter.3}
\contentsline {section}{\numberline {3.1}Sistemas de controle no LNLS}{33}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}EPICS}{33}{subsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.1.1}Exemplo conceitual de Input/Output Controller(IOC)}{34}{subsubsection.3.1.1.1}
\contentsline {subsection}{\numberline {3.1.2}Python}{36}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Sistema de controle da varredura na TXM}{37}{section.3.2}
\contentsline {section}{\numberline {3.3}Scripts de varredura na TXM}{38}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}\textit {Vambora.sh}}{40}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}\textit {HexapodeClass.py}}{41}{subsection.3.3.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Metodologia}}{43}{chapter.4}
\contentsline {section}{\numberline {4.1}Imageamento 2D por florescência}{44}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Configurações do Aparato Experimental}{44}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Movimentação e Controle}{44}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Aquisição}{44}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Integração com a Aquisição}{45}{subsection.4.1.4}
\contentsline {section}{\numberline {4.2} Implementação e testes da Tomografia}{45}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Configurações do Aparato Experimental}{45}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Integração}{45}{subsection.4.2.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Resultados}}{46}{chapter.5}
\contentsline {section}{\numberline {5.1}Instalação e integração dos dispositivos na linha de Luz SXS}{46}{section.5.1}
\contentsline {section}{\numberline {5.2}Configurações do Aparato Experimental}{47}{section.5.2}
\contentsline {section}{\numberline {5.3}Aquisição}{47}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Controle da Aquisição}{48}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Visualização e Interface Gráfica}{49}{subsection.5.3.2}
\contentsline {section}{\numberline {5.4}Movimentação e Controle}{50}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Análise dos modos de movimento do Hexápode}{50}{subsection.5.4.1}
\contentsline {subsubsection}{\numberline {5.4.1.1}Movimento Único}{51}{subsubsection.5.4.1.1}
\contentsline {subsubsection}{\numberline {5.4.1.2}Movimento Sequencial}{54}{subsubsection.5.4.1.2}
\contentsline {subsection}{\numberline {5.4.2}Comparando a estrutura do \textit {Vambora.sh} e \textit {HexapodeClass.py}}{57}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Propostas para a movimentação nos eixos}{59}{subsection.5.4.3}
\contentsline {subsection}{\numberline {5.4.4}Scripts e Modos de Movimento Propostos}{61}{subsection.5.4.4}
\contentsline {subsubsection}{\numberline {5.4.4.1}Proposta 1:Classe$\_$Hexapode.py}{61}{subsubsection.5.4.4.1}
\contentsline {subsubsection}{\numberline {5.4.4.2}Proposta 2: rumo$\_$ao$\_$hexa.py}{61}{subsubsection.5.4.4.2}
\contentsline {subsection}{\numberline {5.4.5}Comparação do desempenho em Tempo Morto dos Scripts Anteriores com os Scripts Propostos}{63}{subsection.5.4.5}
\contentsline {section}{\numberline {5.5}Resultados Preliminares de Imageamento}{64}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Resultados Preliminares de Aquisições realizadas com as soluções propostas para o imageamento 2D}{64}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Resultados Preliminares de Tomografia por Fluorescência}{66}{subsection.5.5.2}
\contentsline {section}{\numberline {5.6}Integração e Interface de Controle}{72}{section.5.6}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Perspectivas Futuras}}{76}{chapter.6}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{CONCLUSÃO}{79}{chapter*.26}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{\uppercase {Refer\^encias}}{81}{section*.29}
\cftinsert {AAA}
\renewcommand *{\cftappendixname }{ANEXO \space }
\setlength {\cftpartindent }{\cftlastnumwidth } \setlength {\cftpartnumwidth }{2em}
\cftinsert {A}
\contentsline {part}{\uppercase {Anexos}}{84}{section*.30}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\cftinsert {A}
\contentsline {appendix}{\chapternumberline {A}\MakeTextUppercase {Informações sobre os motores}}{86}{appendix.anexochapback.1}
