
#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python
#progra1.py

import numpy as np
import os

#INPUT VARIABLES
print("Insira o valor iniciais de x0, y0 e z0")
x0=float(input("x0="))
y0=float(input("y0="))
z0=float(input("z0="))

print("Insira o valores finais de xf, yf e zf")
xf=float(input("xf="))
yf=float(input("yf="))
zf=float(input("zf="))

print("Portanto o range será (mm)")
print('Em x de',x0, 'a', xf, 'mm')
print('Em y de',y0, 'a', yf, 'mm')
print('Em x de',z0, 'a', zf, 'mm')

b="yes"
if b=="no":
    print("ERRRRRRROOOOU")
else: 
    print("STEPS")
print("Quanto de passo para x, y e z?)")
nx=input("n passos em x=")
ny=input("n passos em y=")
nz=input("n passos em z=")
#nx=5
#ny=5
#nz=5
print("Portanto os steps terão tamanho em mm de")
print((xf-x0)/float(nx))
print((yf-y0)/float(ny))
print((zf-z0)/float(nz))
hx=(xf-x0)/float(nx)
hy=(yf-y0)/float(ny)
hz=(zf-z0)/float(nz)
#a=input("Deseja continuar?(yes/no)")
a="yes"
if a=="no":
    print("ERRRRRRROOOOU")
else: 
    print("Gerando o arquivo")
    print("*        *  ")
    print("*        *  ")
#ANALYSING THE INPUTS
ex=1
ey=1
ez=1
if x0==xf:
    ex=0

if y0==yf:
    ey=0

if z0==zf:
    ez=0

        
#PRE ALLOCATE MATRIX
n=float(int(nx)*int(ex)*(int(ny)*int(ey)+1)*(int(nz)*int(ez)+1)+int(ny)*int(ey)*int(ez)*(int(nz)+1)+int(nz)*int(ez))
print(n)
matrix = np.zeros(shape=(int(n),6)) # Pre-allocate matrix

#LOOP TO FILL 
m=0
while(m<(int(n))):
    for i in range(0,int(nx)*int(ex)+1):
        for j in range(0,int(ny)*int(ey)+1):
            for k in range(0,int(nz)*int(ez)+1):
                #print(m,'m',i,'i',j,'j',k,'k')
                matrix[m-1,:]=[x0+(i)*hx,y0+(j)*hy,z0+(k)*hz,0,0,0]
                m+=1
                        
                        #SAVING DATA
data=matrix
datafile_path = "output/sequencia-teste1.txt"
with open(datafile_path, 'w+') as datafile_id:
        #here you open the ascii file
        
    np.savetxt(datafile_id, data,delimiter="\t", fmt=['%f','%f','%f','%f','%f','%f'])
    #here the ascii file is written.

        
print("O arquivo gerado é")
print("*        *  ")
print("*        *  ")
print("________________________________________________________________________________________")
print('  x \t \t   y \t \t   z \t \t   Rx \t \t   Ry \t \t   Rz')
print("________________________________________________________________________________________")
os.system("cat output/sequencia-teste1.txt")
