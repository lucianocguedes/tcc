#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

#analise_hexa.py
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image

fname = "teste1.mca"
fname2 = "teste2.mca"
fname3 = "medida.mca"
fname4 = "teste4.mca"
fname5 = "malha_grande2.mca"
a = np.loadtxt(fname3, skiprows=1 ,usecols=range(5))
#a2 = np.loadtxt(fname3, usecols=range(5))
b=a[:,0].size
#b2=a2[:,0].size
ny=20
nz=6
casa=np.zeros([b,10])
#print(b2)
m=0
y0=-0.09
z0=-0.1
hy=0.01
hz=0.01
while(m<20):
        for k in range(0,int(nz)):
            for j in range(0,int(ny)):
                casa[m,:]=[y0+(j)*hy,z0+(k)*hz,0,0,0,0,0,0,0,m]
                m+=1
casa[:,2:4]=a[:,2:4]
casa[:,7]=abs(casa[:,0]-casa[:,2])/casa[:,0]
casa[:,8]=abs(casa[:,1]-casa[:,3])/casa[:,1]
#casa[:,2:7,1]=a2
#casa[:,7,1]=abs(casa[:,0,1]-casa[:,2,1])/casa[:,0,1]
#casa[:,8,1]=abs(casa[:,1,1]-casa[:,3,1])/casa[:,1,1]
print(casa)
#casa[:,3]=a[:,0]
#casa[:,4]=a[:,1]
#casa[:,5]=a[:,3]
#plt.plot(casa[:,0],casa[:,1],'-ro')
#plt.plot(casa[:,2],casa[:,3],'-go')
#plt.plot(casa[:,2,1],casa[:,3,1],'-go')
#plt.scatter(casa[:,0],casa[:,1],c=casa[:,9],cmap='viridis',marker='x',linestyle='-')
plt.plot(casa[:,0],casa[:,1],'ko',label='#PARAM:')
#plt.scatter(casa[:,2],casa[:,3],c=casa[:,9],cmap='viridis',marker='*',linestyle='-')
plt.plot(casa[:,2],casa[:,3],':xr',label=':POSUSER')
plt.xlabel('Y (mm)')
plt.ylabel('Z (mm)')
plt.legend(loc='best')
#plt.plot(casa[:,2],casa[:,3],'*b')
plt.show(1)

#plt.errorbar(casa[0:11],casa[0:11], casa[0:11,7],casa[0:11,8],
            #fmt='o', ecolor='g', capthick=2)
#plt.show(2)
