#!/usr/local/bin/python3
#gera_mesh.py
import numpy as np
import os

#INPUT VARIABLES
print("Insira o valor iniciais de y0 e z0")
y0=float(input("y0="))
z0=float(input("z0="))
x0=0
print("Insira o valores finais de yf e zf")
xf=0
yf=float(input("yf="))
zf=float(input("zf="))
#y0=-1
#yf=1
#z0=-1
#zf=1
print("Portanto o range será (mm)")
print('Em x de',x0, 'a', xf, 'mm')
print('Em y de',y0, 'a', yf, 'mm')
print('Em x de',z0, 'a', zf, 'mm')
#nx=input("n passos em x=")
ny=input("n passos em y=")
nz=input("n passos em z=")
nx=1
#ny=10
#nz=10
tempo_int=float(input("tempo de medida :"))
tempo=str(tempo_int+0.5)
print(tempo)
print("Portanto os steps terão tamanho em mm de")
hx=(xf-x0)/float(nx)
hy=(yf-y0)/float(ny)
hz=(zf-z0)/float(nz)
print(hx,hy,hz)


new_path = '/home/ABTLUS/sxs/bash_scripts/extra'
with open(new_path,'w') as f:
	f.write("rm -f /home/ABTLUS/sxs/bash_scripts/sym_hexapy/tabela/medida.mca\n")
	f.write("touch /home/ABTLUS/sxs/bash_scripts/sym_hexapy/tabela/medida.mca\n")
	f.write('echo  "*col  row  SYMETRIEY  SYMETRIEZ  i_anel  Si  Au  X" >> /home/ABTLUS/sxs/bash_scripts/sym_hexapy/tabela/medida.mca\n')
	f.write("caput SXS:SCALER.TP "+tempo+" >> /dev/null\n")
	for k in range(0,int(nz)+1):
		pz=float("{0:.4f}".format(z0+(k)*hz))
		pzs=str(pz)
		f.write('\n')
		f.write("caput SYMETRIE:MOVE#PARAM:Z "+pzs+" >> /dev/null"+'\n')
		for j in range(0,int(ny)+1):
			if j==0:
				py=float("{0:.4f}".format(y0+(j)*hy))
				pys=str(py)
				f.write("caput SYMETRIE:MOVE#PARAM:Y "+pys+" >> /dev/null&"+'\n')
				f.write("sleep 5 &"+'\n')
				f.write('wait\n')
				f.write("counter=$((counter+1))"+'\n')
				f.write("valores=`caget -t SYMETRIE:POSUSER:Y SYMETRIE:POSUSER:Z LNLS:ANEL:corrente`"+'\n') 
				f.write("echo $valores $counter"+'\n')
				continue
			f.write('START_l=$(date +%s%N | cut -b1-13)\n')
			py=float("{0:.4f}".format(y0+(j)*hy))
			pys=str(py)
			f.write("caput SYMETRIE:MOVE#PARAM:Y "+pys+" >> /dev/null&"+'\n')
##############			
			f.write("sleep 0.5 &"+'\n')
			f.write('wait\n')
			f.write('caput SXS:SCALER.CNT 1 >> /dev/null&\n')
			f.write("sleep 1.5 &"+'\n')
			f.write('wait\n')
			f.write('caput SXS:SCALER.CNT 0 >> /dev/null\n')
			f.write("counter=$((counter+1))"+'\n')
			f.write("valores=`caget -t SYMETRIE:POSUSER:Y SYMETRIE:POSUSER:Z LNLS:ANEL:corrente SXS:SCALER.S1 SXS:SCALER.S2 SXS:SCALER.S3`"+'\n') 
#################			
			f.write("echo $valores $counter"+'\n')
			j=str(j)
			k=str(k)
			f.write('j='+j+'\n')
			f.write('k='+k+'\n')
			f.write("echo  $j $k $valores|sed 's/ \+ /\\t/g' >> /home/ABTLUS/sxs/bash_scripts/sym_hexapy/tabela/medida.mca\n")
			f.write("END_l=$(date +%s%N | cut -b1-13)\n")
			f.write("DIFF_l=$(( $END_l - $START_l))\n")
			f.write('echo "Levou $DIFF_l ms para o ponto"\n')
	f.write("END=$(date +%s%N | cut -b1-13)"+'\n')
	f.write("caput $PVhexapodSET 0 >> /dev/null"+'\n')
	f.write("DIFF=$(( $END - $START ))\n")
	f.write('echo "Levou $DIFF ms para mudar terminar tudo$index_novo"\n')
	#f.write('exit')


f.close()


datafile_path = "/home/ABTLUS/sxs/bash_scripts/hexa.sh"


os.system("touch /home/ABTLUS/sxs/bash_scripts/hexa_integrado.sh")
os.system("chmod +x /home/ABTLUS/sxs/bash_scripts/hexa_integrado.sh")
os.system("cp /home/ABTLUS/sxs/bash_scripts/hexa.sh /home/ABTLUS/sxs/bash_scripts/hexa_integrado.sh")
os.system("cat /home/ABTLUS/sxs/bash_scripts/extra >> /home/ABTLUS/sxs/bash_scripts/hexa_integrado.sh")
os.system("sh /home/ABTLUS/sxs/bash_scripts/hexa_integrado.sh")

