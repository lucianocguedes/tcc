# # # # # # # # # # # # # # # # # # # # # #
# HEXAPODE INTEGRADO                      		#
# Script para varredura automatizada de   		#
# motores do hexapode Bora              			#
#    LUCIANO GUEDES
#adaptado de James Piton - SOL/LNLS - 27-04-18#
# # # # # # # # # # # # # # # # # # # # # #


# PVs de selecao do modo (param. Q70)
PVeixohexapodMODO="SYMETRIE:MOVE#PARAM:CM"

# PVs da posicao atual dos eixos
PVeixohexapodX="SYMETRIE:POSUSER:X"
PVeixohexapodY="SYMETRIE:POSUSER:Y"
PVeixohexapodZ="SYMETRIE:POSUSER:Z"
PVeixohexapodRX="SYMETRIE:POSUSER:RX"
PVeixohexapodRY="SYMETRIE:POSUSER:RY"
PVeixohexapodRZ="SYMETRIE:POSUSER:RZ"

# Parte inicial dos nomes das PVs que sao a posicao alvo 
PValvohexapod="SYMETRIE:MOVE#PARAM:"

#PVs de Sequencia
PV_seq_index="SYMETRIE:SEQ#INDEX"
PV_seq_run="SYMETRIE:SEQ#RUNNING"


#Pvs externas
PVenergiamono="SXS:MONO:energy_RBV"
PVcontadoras="SXS:SCALER"
PVI0="SXS:K6514A:Measure"
PVKeithley6514="SXS:K6514B:Measure"

# PVs de controle dos comandos para o hexapode
PVhexapodSET="SYMETRIE:STATE#PANEL:SET"
PVhexapodGET="SYMETRIE:STATE#PANEL:GET"

viacaget()
{
# a quinta parte lida do caget indica se ha alarme na PV
# $1: PV
# $2: variavel em que sera devolvido o .VAL da PV, quando nao houver alarme
var2=$2
var5="X"
var4=""
# Modificado por douglas.beniz; estava salvando valores em branco no arquivo final, que nao era aberto pelo pymca
while [[ "$var5" != "" && "$var4" == "" ]]
do
  v=`/usr/local/epics/base/bin/linux-x86_64/caget -ac $1`
  set $v
  var4=$4
  var5=$5
 #if [ "$var5" != "" ]
 # then
 #    echo "PV com erro no IOC! nova leitura"
 # fi 
done
eval "$var2=$4"
}

# garante que o hexapode receberá comandos
caput SYMETRIE:STATE#PANEL:BUTTON 0 >> /dev/null
caput $PVhexapodSET 0 >> /dev/null

# seleciona o modo de movimento, quanto ao sistema de coordenadas
# MODO de coordenada (0: absoluta, 1: Rel. ao Objeto, 2: Rel. ao Usuario)
MODOMOVIMENTO="0"

caput $PVeixohexapodMODO $MODOMOVIMENTO >> /dev/null

# aguarda a PV dada chegar ao valor dado como segundo parametro
espera () {
 # o terceiro parametro indica se espera pelo valor dado ser IGUAL ou DIFERENTE
 # entao se espera até que a PV atinja o valor
 if [ "$3" == "IGUAL" ]
   then
     #echo "espera ate que $1 seja igual a $2"
     leitura="9999999" 
     while [ "$leitura" != "$2" ]
     do
     #  leitura= `caget -t $PVhexapodGET`
	#viacaget $1 leitura
leitura=`caget -t SYMETRIE:STATE#PANEL:GET`
     done
 else
 # se houver o segundo parametro (DIFERENTE)
 # espera-se até que a PV deixe de ter aquele valor 
     leitura=$2
     #echo "espera ate que $1 seja DIFERENTE de $2"
     while [ "$leitura" == "$2" ]
     do
        #  leitura=`caget -t $PVhexapodGET`
leitura=`caget -t SYMETRIE:STATE#PANEL:GET`
#viacaget $1 leitura     
	done
 fi 
}

# Manda para o hexapode o codigo de comando de uma operacao e 
# aguarda a execucao completar
mandacomando() {
  caput $PVhexapodSET $1 >> /dev/null
  espera $PVhexapodGET 0 'DIFERENTE'
  caput $PVhexapodSET 0 >> /dev/null
  espera $PVhexapodGET 0 'IGUAL'
}

# Preenche as PVs alvos de cada um dos eixos presentes na lista de parâmetros
# A lista eh passada na forma de pares eixo-posicao alvo:
#     moveeixos  X 1.2 Z 3.14 TZ 1
moveeixos() {
  echo -n "Move eixos $1 para $2"
  apontaeixo $1 $2
  if [ ! -z "$3" ]
    then
       echo -n ", $3 para $4"
       apontaeixo $3 $4
       if [ ! -z "$5" ]
         then
            echo -n ", $5 para $6"
            apontaeixo $5 $6
            if [ ! -z "$7" ]
              then
                echo -n ", $7 para $8"
                apontaeixo $7 $8
                if [ ! -z "$9" ]
                  then
                    echo -n ", $9 para $10"
                    apontaeixo $9 $10
                    if [ ! -z "$11" ]
                      then
                        echo -n ", $11 para $12"
                        apontaeixo $11 $12
                    fi
                 fi
            fi
        fi
   fi
  mandacomando 11
}

contagem() {
  # $1: detetor (K=Keithley, C=Contadoras)
  # o retorno se da na variavel cont
  if [ "$1" == "K" ]
    then
	cont=`caget -t $PVKeithley6514`
  else
contadoras=`caget -t SXS:SCALER.S2 SXS:SCALER.S3 SXS:SCALER.S4 SXS:SCALER.S5 SXS:SCALER.S6 SXS:SCALER.S7 SXS:SCALER.S8 SXS:SCALER.S9`
s2=`echo $contadoras | awk '{print $1}'`
s3=`echo $contadoras | awk '{print $2}'`
s4=`echo $contadoras | awk '{print $3}'`
s5=`echo $contadoras | awk '{print $4}'`
s6=`echo $contadoras | awk '{print $5}'`
s7=`echo $contadoras | awk '{print $6}'`
s8=`echo $contadoras | awk '{print $7}'`
s9=`echo $contadoras | awk '{print $8}'`
    cont=`echo -e "$s2\t$s3\t$s4\t$s5\t$s6\t$s7\t$s8\t$s9"`
  fi 
}


# douglas.beniz (SOL)
gera_cabecalho_arquivo_malha_pymca() {
  echo -e "*row\tcol\tZ\tY\tI0\tI1\t$posS2(S2)\t$posS3(S3)\t$posS4(S4)\t$posS5(S5)\t$posS6(S6)\t$posS7(S7)\t$posS8(S8)\t$posS9(S9)" >> $arquivo
}

gera_cabecalho_arquivo_malha() {
# cria cabecalho e todo o formato de arquivo .dat para o programa Smak
  echo -e "* Abscissa points :\t$posptsY" >> $arquivo
  echo -e "* Ordinate points :\t$posptsZ" >> $arquivo
  echo "* "  >> $arquivo
  echo -e "* Data Channels :\t6"  >> $arquivo
  echo -e "* Data Labels :\tI0\tI1\t$posS2\t$posS3\t$posS4\t$posS5"  >> $arquivo
  echo "* Comments:"  >> $arquivo
  echo "*" >> $arquivo
  echo "*" >> $arquivo
  echo "* " >> $arquivo
  echo "* " >> $arquivo
  echo "* Abscissa points requested :" >> $arquivo
  echo -n "* " >> $arquivo

  ctiY=0
  ponto=$posiniY
  while [[ "$ctiY" -lt "$posptsY" && "$_PARA" -ne "1" ]]
  do
     printf '%f' "$ponto" >> $arquivo
     ponto=`echo "scale=4; $ponto + $passoY" | bc -l`
     ctiY=`expr $ctiY + 1`
     if [ "$ctiY" -lt "$posptsY" ]
       then
         echo -en "\t" >> $arquivo
     else
       echo >> $arquivo
     fi
  done

  echo "* " >> $arquivo
  echo "* " >> $arquivo
  echo "* Ordinate points requested :" >> $arquivo
  echo -n "* " >> $arquivo

  ctiZ=0
  ponto=$posiniZ
  while [[ "$ctiZ" -lt "$posptsZ" && "$_PARA" -ne "1" ]]
  do
     printf '%f' "$ponto" >> $arquivo
     ponto=`echo "scale=4; $ponto + $passoZ" | bc -l`
     ctiZ=`expr $ctiZ + 1`
     if [ "$ctiZ" -lt "$posptsZ" ]
       then
         echo -en "\t" >> $arquivo
     else
       echo >> $arquivo
     fi
  done

  echo "* " >> $arquivo
  echo "* " >> $arquivo

  viacaget $PVenergiamono energia
  echo "* Energy points requested:" >> $arquivo
  echo -e "*\t$energia" >> $arquivo

  echo "* " >> $arquivo
  echo "* DATA" >> $arquivo
}
mandaseq() {
caput SYMETRIE:MOVE#PARAM:Z $1
mandacomando 11
espera SYMETRIE:STATE#HEXA? 30 IGUAL
caget SYMETRIE:POSUSER:Z
echo "ja moveu Z"
n_y=`cat /home/ABTLUS/sxs/bash_scripts/sym_hexapy/sequencia.txt|wc -l`
valorIanel=`caget -t LNLS:ANEL:corrente`
mandacomando 12

valores2=`caget -t SYMETRIE:POSUSER:Y SYMETRIE:POSUSER:Z LNLS:ANEL:corrente SYMETRIE:STATE#HEXA? SYMETRIE:SEQ#INDEX`
				echo $valores2
				Y=`echo $valores2 | awk '{print $1}'`
				Z=`echo $valores2 | awk '{print $2}'`
				valorIanel=`echo $valores2 | awk '{print $3}'`
				inposition=`echo $valores2 | awk '{print $4}'`
				index_novo=`echo $valores2 | awk '{print $5}'`
				index_ant=$index_novo
		
#echo  "inicio" $Y $Z $valorIanel

while [ $index_ant -lt $n_y ]
do
valores=`caget -t $PV_seq_index SYMETRIE:STATE#HEXA?`
index_novo=`echo $valores| awk '{print $1}'`leitura=`caget -t SYMETRIE:STATE#PANEL:GET`
inposition=`echo $valores | awk '{print $2}'`
  		#if [ "$index_ant" != "$index_novo" ]
		if [ "$index_ant" != "$index_novo" ] && [ "$inposition" == "30" ]
		  then
			START=$(date +%s%N | cut -b1-13)
			valores2=`caget -t SYMETRIE:POSUSER:Y SYMETRIE:POSUSER:Z LNLS:ANEL:corrente SYMETRIE:STATE#HEXA? SYMETRIE:SEQ#INDEX`
				#echo $valores2
				Y=`echo $valores2 | awk '{print $1}'`
				Z=`echo $valores2 | awk '{print $2}'`
				valorIanel=`echo $valores2 | awk '{print $3}'`
				inposition=`echo $valores2 | awk '{print $4}'`
				index_novo=`echo $valores2 | awk '{print $5}'`
				index_ant=$index_novo
			END=$(date +%s%N | cut -b1-13)
			DIFF=$(( $END - $START ))
			echo "Levou $DIFF ms para mudar para a linha $index_novo"
		if [ "$index_novo" -gt "2" ]
		  then
			echo $valores2
		echo  $valores2|sed 's/ \+ /\t/g' >> /home/ABTLUS/sxs/bash_scripts/sym_hexapy/tabela/medida
		fi	     		
fi
done
espera $PV_seq_run 0 IGUAL
echo "terminou a lista"
}



mandair(){

caput SYMETRIE:MOVE#PARAM:Z $3  >> /dev/null&
caput SYMETRIE:MOVE#PARAM:X $1  >> /dev/null&
caput SYMETRIE:MOVE#PARAM:Y $2  >> /dev/null&
wait
inposition=`caget -t SYMETRIE:STATE#HEXA?`

while [ $inposition == 30 ]
do
    inposition=`caget -t SYMETRIE:STATE#HEXA?`
	START_d=$(date +%s%N | cut -b1-13)
	while [ $inposition == 26 ]
		do
			inposition=`caget -t SYMETRIE:STATE#HEXA?`
			if [ "$inposition" -eq "30" ]
				then
 				echo "chegou"
				valores=`caget -t SYMETRIE:POSUSER:X SYMETRIE:POSUSER:Y SYMETRIE:POSUSER:Z`
				echo $valores
			END_d=$(date +%s%N | cut -b1-13)
				break 3
			fi
	done
done
echo "aae porra"



			DIFF_d=$(( $END_d - $START_d ))
			echo "AAAAAAAAAAAAAAAAA$DIFF_d ms "
}
###############################################################################################################
###############################################################################################################
###############################################################################################################
###############################################################################################################
###############################################################################################################
###############################################################################################################
#moveeixos X  -0 Y  -2 Z 0
#espera SYMETRIE:STATE#HEXA? 30 IGUAL
caput $PVhexapodSET 0 >> /dev/null
valores=`caget -t SYMETRIE:POSUSER:X SYMETRIE:POSUSER:Y SYMETRIE:POSUSER:Z`
echo $valores
caput $PVhexapodSET 11 >> /dev/null
valores=`caget -t SYMETRIE:POSUSER:X SYMETRIE:POSUSER:Y SYMETRIE:POSUSER:Z`
echo $valores
START=$(date +%s%N | cut -b1-13)

counter=0
caput SYMETRIE:MOVE#PARAM:Z -1 >> /dev/null&
caput SYMETRIE:MOVE#PARAM:X 0  >> /dev/null&
caput SYMETRIE:MOVE#PARAM:Y 0 >> /dev/null&
wait
sleep 3

