import epics
import sys
import numpy as np
import time



#LOADING LIMIT AXIS
fnamey = "/home/ABTLUS/sxshexapython/input_files/sequencia.txt"
fnamez = "/home/ABTLUS/sxs/hexapython/input_files/eixoz"
tabela = np.loadtxt(fnamey, skiprows=1, usecols=range(3))
eixoz= np.loadtxt(fnamez,skiprows=10, usecols=range(1))
eixoy=tabela[1:,1]
print(eixoz)
print(eixoz[2])

#GLOBAL VARIABLES
j_novo=0
posy=0
posatual=0
a=False
channel1=0
channel2=0
channel3=0
#PV's
mypv = epics.PV('SYMETRIE:POSUSER:Y')
pvscaler_tempo=epics.PV('SXS:SCALER.TP.VAL')
pvscaler_cnt=epics.PV('SXS:SCALER.CNT.VAL')
pvscaler_s1=epics.PV('SXS:SCALER.S1.VAL')
pvscaler_s2=epics.PV('SXS:SCALER.S2.VAL')
pvscaler_s3=epics.PV('SXS:SCALER.S3.VAL')

#FUNCTIONS
def onChanges(pvname=None, value=None, char_value=None, **kw):
#def onChanges(pvname=None, value=None, **kw):

	global posy
	#print ("valor atual: ",char_value)
	if (abs((posy - float(char_value))) < 1.e-4):
		#gets the value of the last point		
		print("  ")
		print(" counting?:")
		print(pvscaler_cnt.value)		
		global channel1
		global channel2
		global channel3
		channel1=pvscaler_s1.value
		channel2=pvscaler_s2.value
		channel3=pvscaler_s3.value
		posatual=char_value
		global j_novo
		global a
		global posatual
		#Starts the cnt
		pvscaler_cnt.put(value=1)
		#next step
		a =True
		j_novo+=1
		
		
		



pvscaler_tempo.put(value=1.00)
mypv.add_callback(onChanges)
print ('Now wait for changes')

#LOCAL VARIABLES
t0 = time.time()
ny=eixoy.size
nz=eixoz.size
inicio = time.time()


#MESH
for k in range(0,nz-1):
	j_novo=0
	j_ant=0
	while(j_novo<ny): 
		posy=eixoy[j_novo]
		#print(posy)
		time.sleep(1.e-4)
		if a and (j_ant!=j_novo):
			print('chegou na posicao',posatual,channel1, channel2)		
			j_ant=j_novo
			fim = time.time() - inicio
			print('tempo',fim)
			inicio = time.time()
			#print(pvscaler_cnt.value)	
#print ('primeira linha feita')
