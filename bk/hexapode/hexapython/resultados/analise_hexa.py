#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

#analise_hexa.py
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image

fname = "teste1.mca"
fname2 = "rt.mca"
fname3 = "res1.mca"
fname4 = "Au40um25pts-20um_0001.mca"
fname5 = "malha_grande2.mca"
a = np.loadtxt(fname, skiprows=1 ,usecols=range(4))
#a2 = np.loadtxt(fname3, usecols=range(5))
b=a[:,0].size
#b2=a2[:,0].size
ny=11
nz=11
casa=np.zeros([460,10])
#print(b2)
m=0
y0=-0.1
yf=0.1
z0=-0.1

hy=0.02
hz=0.02
m=0
k=0
zf=z0+hz*nz

casa[0:b,2:4]=a[:,2:4]

print(casa)
yf=y0+hy*ny
xvalues = np.arange(y0, yf, hy)
yvalues = np.arange(z0, zf, hz)
xx, yy = np.meshgrid(xvalues, yvalues)
plt.plot(a[:,3],a[:,2],':xr',label=':POSUSER')
plt.plot(xx, yy, marker='.', color='k', linestyle='none')
plt.xlabel('Y (mm)')
plt.ylabel('Z (mm)')
plt.legend(loc='best')
#plt.plot(casa[:,2],casa[:,3],'*b')
plt.show(1)

#plt.errorbar(casa[0:11],casa[0:11], casa[0:11,7],casa[0:11,8],
            #fmt='o', ecolor='g', capthick=2)
#plt.show(2)
