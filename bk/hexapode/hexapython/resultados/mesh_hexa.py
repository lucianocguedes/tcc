import epics
import sys
import numpy as np
import time
import os
import SYM_HexaPy
from params import *


#LOADING LIMIT AXIS
fnamey = "/home/ABTLUS/sxs/hexapython/input_files/sequencia.txt"
fnamez = "/home/ABTLUS/sxs/hexapython/input_files/eixoz"
tabela = np.loadtxt(fnamey, skiprows=1, usecols=range(3))
eixoz= np.loadtxt(fnamez, usecols=range(1))
eixoy=tabela[:,1]
eixoztabela=tabela[:,2]
print(eixoz)
print(eixoy[20])

#GLOBAL VARIABLES
j_novo=0
posy=0
posz=0
yposatual=0
zposatual=0
a=False
channel1=0
channel2=0
channel3=0
#PV's
#mypv = epics.PV('SYMETRIE:POSUSER:Y')
z_pos = epics.PV('SYMETRIE:POSUSER:Z.VAL')
y_pos = epics.PV('SYMETRIE:POSUSER:Y.VAL')
y_target=epics.PV('SYMETRIE:MOVE#PARAM:Y')
z_target=epics.PV('SYMETRIE:MOVE#PARAM:Z')
pvscaler_tempo=epics.PV('SXS:SCALER.TP.VAL')
pvscaler_cnt=epics.PV('SXS:SCALER.CNT.VAL')
pvscaler_s1=epics.PV('SXS:SCALER.S1.VAL')
pvscaler_s2=epics.PV('SXS:SCALER.S2.VAL')
pvscaler_s3=epics.PV('SXS:SCALER.S3.VAL')
hexastatus=epics.PV('SYMETRIE:STATE#HEXA?.VAL')
index=epics.PV('SYMETRIE:SEQ#INDEX.VAL')

z_pos_scan = epics.PV('SYMETRIE:POSUSER:Z.SCAN')
y_pos_scan = epics.PV('SYMETRIE:POSUSER:Y.SCAN')
hexastatus_scan=epics.PV('SYMETRIE:STATE#HEXA?.SCAN')
index_scan=epics.PV('SYMETRIE:SEQ#INDEX.SCAN')
###############################################################################################################
#FUNCTIONS
##
def set_time_scan(pvname):
	if pvname.value != "9":
		pvname.put(value=9)
##
def scan_table():
#waiting for stabilization	
	#time.sleep(t_stab)
	cmd_option.put(value=12)
	while(cmd_option.value !=12):
		a=True		
##
def uptable(t_stab,t_mes):
	t_mes=float(t_mes)
	t_stab=float(t_stab)
	#Parameters
	ip = "10.2.97.31"
	port = 1025
	SEQ_file_path = "/home/ABTLUS/sxs/hexapython/input_files/sequencia.txt"
	SEQ_pause_stab = t_stab
	#pause_m= float(input("tempo de medida="))
	SEQ_pause_mes =(t_mes)/2.0
	SEQ_dec_nb = 1


	#Connect to the controller
	hexapod = SYM_HexaPy.api()
	hexapod.connect(ip,port)
	hexapod.term("&2 Q20=2",False) #STOP command
	hexapod.SEQ_download(SEQ_file_path, SEQ_pause_stab, SEQ_pause_mes, SEQ_dec_nb)

	#hexapod.term("&2 Q20=12",False) #START sequence
	hexapod.disconnect()
##

def new_mesh(_option):
	if _option=="yes":
		#INPUT VARIABLES
		print("Insira o valor iniciais de x0, y0 e z0")
		#x0=float(input("x0="))
		x0=0
		xf=0
		nx=1		
		y0=float(input("y0="))
		z0=float(input("z0="))

		print("Insira o valores finais de xf, yf e zf")
		#xf=float(input("xf="))
		yf=float(input("yf="))
		zf=float(input("zf="))

		print("Portanto o range será (mm)")
		print('Em x de',x0, 'a', xf, 'mm')
		print('Em y de',y0, 'a', yf, 'mm')
		print('Em x de',z0, 'a', zf, 'mm')

		b="yes"
		if b=="no":
		    print("ERRRRRRROOOOU")
		else: 
		    print("STEPS")
		print("Quanto de passo para x, y e z?)")
		#nx=input("n passos em x=")
		ny=input("n passos em y=")
		nz=input("n passos em z=")
		tstab=input("tstab=")
		tmes=input("tmes=")
		#nx=5
		#ny=5
		#nz=5
		print("Portanto os steps terão tamanho em mm de")
		print((xf-x0)/float(nx))
		print((yf-y0)/float(ny))
		print((zf-z0)/float(nz))
		hx=(xf-x0)/float(nx)
		hy=(yf-y0)/float(ny)
		hz=(zf-z0)/float(nz)
		#a=input("Deseja continuar?(yes/no)")
		a="yes"
		if a=="no":
		    print("ERRRRRRROOOOU")
		else: 
		    print("Gerando o arquivo")
		    print("*        *  ")
		    print("*        *  ")
		#ANALYSING THE INPUTS
		ex=1
		ey=1
		ez=1
		if x0==xf:
		    ex=0

		if y0==yf:
		    ey=0

		if z0==zf:
		    ez=0

		
		#PRE ALLOCATE MATRIX
		n=float(int(nx)*int(ex)*(int(ny)*int(ey)+1)*(int(nz)*int(ez)+1)+int(ny)*int(ey)*int(ez)*(int(nz)+1)+int(nz)*int(ez))
		print(n)
		matrix = np.zeros(shape=(int(n),6)) # Pre-allocate matrix

		#LOOP TO FILL 
		m=0
		while(m<(int(n))):
			for k in range(0,int(nz)*int(ez)+1):
				for i in range(0,int(nx)*int(ex)+1):
					for j in range(0,int(ny)*int(ey)+1):
						#	for k in range(0,int(nz)*int(ez)+1):
						#print(m,'m',i,'i',j,'j',k,'k')
						matrix[m-1,:]=[x0+(i)*hx,y0+(j)*hy,z0+(k)*hz,0,0,0]
						m+=1
		#SAVING DATA
		data=matrix
		datafile_path = "/home/ABTLUS/sxs/hexapython/input_files/sequencia.txt"
		with open(datafile_path, 'wb') as datafile_id:
			np.savetxt(datafile_id, data,delimiter="\t", fmt=['%.4f','%.4f','%.4f','%.4f','%.4f','%.4f'])


		#For z axis

		datafile_path = "/home/ABTLUS/sxs/hexapython/input_files/eixoz"
		eixoz = np.zeros((int(nz)+1)) 
		m=0
		for k in range(0,int(nz)+1):
			eixoz[k]=z0+(k)*hz
		dataz=eixoz
		with open(datafile_path, 'wb') as datafile_id:
			np.savetxt(datafile_id, dataz, fmt=['%.4f'])

		params_path = "/home/ABTLUS/sxs/hexapython/params.py"
		with open(params_path, 'wb') as datafile_id:
			datafile_id.write(("y0=" + str(y0)+ "\n").encode("utf8"))
			datafile_id.write(('yf='+str(yf)+'\n').encode("utf8"))
			datafile_id.write(('ny='+str(ny)+'\n').encode("utf8"))
			datafile_id.write(('z0='+str(z0)+'\n').encode("utf8")) 
			datafile_id.write(('zf='+str(zf)+'\n').encode("utf8")) 
			datafile_id.write(('nz='+str(nz)+'\n').encode("utf8")) 
			datafile_id.write(('tstab='+str(tstab)+'\n').encode("utf8")) 
			datafile_id.write(('tmes='+str(tmes)+'\n').encode("utf8")) 
			datafile_id.write(('hy='+str(hy)+'\n').encode("utf8")) 
			datafile_id.write(('hz='+str(hz)+'\n').encode("utf8"))
		print("O arquivo gerado é")
		print("*        *  ")
		print("*        *  ")
		print("________________________________________________________________________________________")
		print('  x \t \t   y \t \t   z \t \t   Rx \t \t   Ry \t \t   Rz')
		print("________________________________________________________________________________________")
		os.system("cat /home/ABTLUS/sxs/hexapython/input_files/sequencia.txt")
		print("*        *  ")
		print("*        *  ")
		print("Z axis:")
		os.system("cat /home/ABTLUS/sxs/hexapython/input_files/eixoz")
		print("*        *  ")
		print("*        *  ")
	
	elif _option=="no":
		print(" the actual table is :")
		print("________________________________________________________________________________________")
		print('  x \t \t   y \t \t   z \t \t   Rx \t \t   Ry \t \t   Rz')
		print("________________________________________________________________________________________")
		os.system("cat /home/ABTLUS/sxs/hexapython/input_files/sequencia.txt")
		print("*        *  ")
		print("*        *  ")
		print("Z axis:")
		os.system("cat /home/ABTLUS/sxs/hexapython/input_files/eixoz")
		print("*        *  ")
		print("*        *  ")
		#uptable(tstab,tmes)
	else:
		print("wrong option")
		sys.exit()
##
'''def onChanges(pvname=None, value=None, char_value=None, **kw):
#'''
index_ant=0
def onChanges_index(pvname=None, value=None, char_value=None, **kw):
	global index_ant
	global eixoy
	global eixoztabela
	index_novo=char_value
	if index_novo!=index_ant:
		valory=eixoy[int(index_novo)-1]
		valorz=eixoztabela[int(index_novo)-1]
		print("mudou de linha",valory)
		y_target.put(value=valory)
		z_target.put(value=valorz)
		index_ant=index_novo
		'''#gets the value of the last point		
		print("  ")
		print(pvscaler_cnt.value)		
		global channel1
		global channel2
		global channel3
		global yposatual
		global zposatual
		channel1=pvscaler_s1.value
		channel2=pvscaler_s2.value
		channel3=pvscaler_s3.value
		yposatual=char_value
		zposatual=zpos_teste
		global j_novo
		global a
		#Starts the cnt
		pvscaler_cnt.put(value=1)
		#next step
		a =True
		j_novo+=1
		'''
##
inicio = time.time()
def onChanges(pvname=None, value=None, char_value=None, **kw):
	global posz
	global posy
	global inicio
	if (char_value == "30") or (((time.time() - inicio)>2) and (float(y_pos.value)>-0.8)):
		global yposatual
		global zposatual
		global j_novo
		global a
		a =True
		yposatual=y_pos.value
		zposatual=z_pos.value
		fim = time.time() - inicio
		inicio = time.time()
		#print ("valor atual: ",yposatual,zposatual,fim)
		if fim>2.2:
			print("  ")
			print("*")
		j_novo+=1

#######################################
#######################################
#######################################
########################################
#########################################
pvscaler_tempo.put(value=1.00)
hexastatus.add_callback(onChanges)
index.add_callback(onChanges_index)
print ('Now wait for changes')
name1="X"
name2="Si"
#LOCAL VARIABLES
t0 = time.time()
from params import *
#ny=eixoy.size
#nz=eixoz.size
inicio = time.time()
print(ny)
#matrix=np.zeros(shape=(int(),6)) 
'''while 1:
	a=True
'''
new_path = '/home/ABTLUS/sxs/hexapython/resultados/mesh_001.mca'
with open(new_path,'w') as f:
	f.write('*col row  SYMETRIEZ  SYMETRIEY '+str(name1)+' '+str(name2)+'X\n')
	#MESH
	for k in range(0,(nz+1)):
		j_novo=0
		j_ant=0
		print(k)
		posz=eixoz[k]
		while(j_novo<ny): 
			posy=eixoy[j_novo]
			#print(posy)
			#time.sleep(1.e-4)
			if a and (j_ant!=j_novo):
				#zposatual=z_pos.value
				f.write(str(k)+'\t'+str(j_novo)+'\t'+str(zposatual)+'\t'+str(yposatual)+'\t'+str(channel1)+'\t'+str( channel2)+'\n')
				print(k, j_novo, zposatual, yposatual)	
				j_ant=j_novo
				#fim = time.time() - inicio
				#print('tempo',fim)
				#inicio = time.time()
				#print(pvscaler_cnt.value)	
f.close()
#print ('primeira linha feita')
