import epics
import sys
import numpy as np
import time
from params import *

#LOADING LIMIT AXIS
fnamey = "/home/ABTLUS/sxs/hexapython/input_files/sequencia.txt"
fnamez = "/home/ABTLUS/sxs/hexapython/input_files/eixoz"
tabela = np.loadtxt(fnamey, skiprows=1, usecols=range(3))
eixoz= np.loadtxt(fnamez, usecols=range(1))
eixoy=tabela[:,1]
eixoztabela=tabela[:,2]
print(eixoz)
print(eixoy[20])

#GLOBAL VARIABLES
j_novo=0
posy=0
posz=0
yposatual=0
zposatual=0
a=False
channel1=0
channel2=0
channel3=0

#PV's
#mypv = epics.PV('SYMETRIE:POSUSER:Y')
z_pos = epics.PV('SYMETRIE:POSUSER:Z.VAL')
y_pos = epics.PV('SYMETRIE:POSUSER:Y.VAL')
y_target=epics.PV('SYMETRIE:MOVE#PARAM:Y')
z_target=epics.PV('SYMETRIE:MOVE#PARAM:Z')
pvscaler_tempo=epics.PV('SXS:SCALER.TP.VAL')
pvscaler_cnt=epics.PV('SXS:SCALER.CNT.VAL')
pvscaler_s1=epics.PV('SXS:SCALER.S1.VAL')
pvscaler_s2=epics.PV('SXS:SCALER.S2.VAL')
pvscaler_s3=epics.PV('SXS:SCALER.S3.VAL')
hexastatus=epics.PV('SYMETRIE:STATE#HEXA?.VAL')
index=epics.PV('SYMETRIE:SEQ#INDEX.VAL')

z_pos_scan = epics.PV('SYMETRIE:POSUSER:Z.SCAN')
y_pos_scan = epics.PV('SYMETRIE:POSUSER:Y.SCAN')
hexastatus_scan=epics.PV('SYMETRIE:STATE#HEXA?.SCAN')
index_scan=epics.PV('SYMETRIE:SEQ#INDEX.SCAN')


#FUNCTIONS
def set_time_scan(pvname):
	if pvname.value != "9":
		pvname.put(value=9)
##
index_ant=0
def onChanges_index(pvname=None, value=None, char_value=None, **kw):
	global index_ant
	global eixoy
	global eixoztabela
	index_novo=char_value
	if int(index_novo)-int(index_ant)>1:
		print("perdeu*******************************ponto")
	if index_novo!=index_ant:
		print("a1")
		valory=eixoy[int(index_novo)-1]
		print("a2")
		valorz=eixoztabela[int(index_novo)-2]
		print("a3")
		print("mudou de linha",valorz)
		print("a4")
		y_target.put(value=valory)
		print("a5")
		z_target.put(value=valorz)
		print("a6")
		index_ant=index_novo

	
inicio = time.time()
def onChanges(pvname=None, value=None, char_value=None, **kw):
	global posz
	global posy
	global inicio
	tempo=time.time() 
	if (char_value == "30") or (((tempo - inicio)>2)):
		global yposatual
		global zposatual
		global j_novo
		global a
		a =True
		yposatual=y_pos.value
		zposatual=z_pos.value
		inicio = time.time()
		global channel1
		global channel2
		global channel3
		channel1=pvscaler_s1.value
		channel2=pvscaler_s2.value
		channel3=pvscaler_s3.value
		global j_novo
		global a
		#Starts the cnt
		pvscaler_cnt.put(value=1)
		#print ("valor atual: ",yposatual,zposatual,fim)
		'''if >2.2:
			print("  ")
			print("*")'''
		j_novo+=1

##################
#####################
'''
#mypv = epics.PV('SYMETRIE:POSUSER:Y')
z_pos = epics.PV('SYMETRIE:POSUSER:Z.VAL')
y_pos = epics.PV('SYMETRIE:POSUSER:Y.VAL')
y_target=epics.PV('SYMETRIE:MOVE#PARAM:Y')
z_target=epics.PV('SYMETRIE:MOVE#PARAM:Z')
pvscaler_tempo=epics.PV('SXS:SCALER.TP.VAL')
pvscaler_cnt=epics.PV('SXS:SCALER.CNT.VAL')
pvscaler_s1=epics.PV('SXS:SCALER.S1.VAL')
pvscaler_s2=epics.PV('SXS:SCALER.S2.VAL')
pvscaler_s3=epics.PV('SXS:SCALER.S3.VAL')
hexastatus=epics.PV('SYMETRIE:STATE#HEXA?.VAL')
index=epics.PV('SYMETRIE:SEQ#INDEX.VAL')

z_pos_scan = epics.PV('SYMETRIE:POSUSER:Z.SCAN')
y_pos_scan = epics.PV('SYMETRIE:POSUSER:Y.SCAN')
hexastatus_scan=epics.PV('SYMETRIE:STATE#HEXA?.SCAN')
index_scan=epics.PV('SYMETRIE:SEQ#INDEX.SCAN')
'''

#1)Set scans to .1second
set_time_scan(z_pos_scan)
set_time_scan(y_pos_scan)
set_time_scan(hexastatus_scan)
set_time_scan(index_scan)
#1.1)Set time scaler mesu
pvscaler_tempo.put(value=1.00)
#2)Initiates the callback functions
hexastatus.add_callback(onChanges)
index.add_callback(onChanges_index)

print ('Now wait for changes')
#LOCAL VARIABLES
name1="X"
name2="Si"
t0 = time.time()
#from params import *
inicio = time.time()
print("ny= ",ny)

new_path = '/home/ABTLUS/sxs/hexapython/resultados/mesh_001.mca'
with open(new_path,'w') as f:
	f.write('*col row  SYMETRIEZ  SYMETRIEY '+str(name1)+' '+str(name2)+'X\n')
	#MESH
	for k in range(0,2*(nz+2)):
		j_novo=0
		j_ant=0
		print("k= ",k)
		posz=eixoz[k]
		while(j_novo<ny): 
			posy=eixoy[j_novo]
			if a and (j_ant!=j_novo):
				if (k==0) and (j_novo==1):
					continue
				else:
					f.write(str(k)+'\t'+str(j_novo)+'\t'+str(zposatual)+'\t'+str(yposatual)+'\t'+str(channel1)+'\t'+str( channel2)+'\n')
					print(k, (j_novo), zposatual, yposatual)	
				j_ant=j_novo
				#print(pvscaler_cnt.value)	
f.close()
