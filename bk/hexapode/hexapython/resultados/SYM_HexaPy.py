'''
Some of the code below is based on the dls_pmaclib Python library that was made available 
as open source by Diamond Control under the LGPL v3.x. For more info and the original 
source code, please go to http://controls.diamond.ac.uk/downloads/python/index.php].
'''

#!/usr/bin/python3

import sys, re, socket, struct
import threading, time
from csv import reader

class PmacEthernetInterface:
	'''Allows connection to a PMAC over an Ethernet interface.'''
	def __init__(self, verbose = False):
		# Basic connection settings
		self.verboseMode = verbose
		self.hostname = ""
		self.port = None
		
		# Access-to-the-connection semaphore. Use this to lock/unlock I/O access to the connection (whatever type it is) in child classes.
		self.semaphore = threading.Semaphore()
		
		self.isConnectionOpen = False
		
	# Attempts to open a connection to a remote PMAC.
	# Returns None on success, or an error message string on failure.
	def connect(self, host = "localhost", port = None):
		
		self.hostname = str(host)
		if port:
			self.port = int(str(port))
		else: port = None
		
		# Sanity checks
		if self.isConnectionOpen:
			return 'Socket is already open'
		if self.hostname in (None, '') or self.port in (None, 0):
			return 'ERROR: hostname or port number not set'

		# Create a new socket instance
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.setblocking(1)
		self.sock.settimeout(3)

		# Attempt to establish a connection to the remote host
		try:
			if self.verboseMode:
				print ('Connecting to host "%s" using port %d' % (self.hostname, self.port))
			self.sock.connect( (self.hostname, self.port) )
		except socket.gaierror:
			return 'ERROR: unknown host'
		except:
			return 'ERROR: connection refused by host'

		self.isConnectionOpen = True
		
		# Check that the we are connected to a pmac by issuing the "ver" command
		# If we don't get the right response, then disconnect automatically
		try:
			response = self.sendCommand('ver')
		except IOError:
			self.disconnect()
			return 'Device failed to respond to a "ver" command'
		if not re.match('^\d+\.\d+\s*\r\x06$',response):
			# if the response is not of the form "1.945  \r\x06" then we're not talking to a PMAC!
			self.disconnect()
			return 'Device did not respond correctly to a "ver" command'
			
	# Disconnect from the telnet session
	# Returns None on success; error message on failure.
	def disconnect(self):
		if self.isConnectionOpen:
			self.semaphore.acquire()
			self.sock.close()
			self.semaphore.release()
			self.isConnectionOpen = False
			if self.verboseMode:
				print ('Disconnected from ' + self.hostname)

        # Send a single command to the controller and block until a response from the controller.       
	def sendCommand(self, command, answer_nb = 1, shouldWait = True):
		def addHeader(command):
			assert type(command) == str
			#in python 3, struct.pack will encode the string data into bytes
			headerStr = struct.pack('8B',0x40,0xBF,0x0,0x0,0x0,0x0,0x0,len(command))	
			#we decode the command to get a string
			wrappedCommand = headerStr + command.encode(encoding='windows-1252')
			return wrappedCommand
		try:
			try:
				if shouldWait:
					self.semaphore.acquire()
				#in python 3, sock.sendall need bytes as parameters
				#print (addHeader(command))
				self.sock.sendall(addHeader(command))    # attept to send the whole packet (as bytes)                              
                                
				#in python 3, recv gets data as bytes so we need to decode it
				returnStr = self.sock.recv(2048).decode(encoding='windows-1252')# wait for and read the response from PMAC (will be at most 1400 chars)
			#	print(returnStr)
				return returnStr
			finally:
				if shouldWait:
					self.semaphore.release()
		except socket.error:
			# Interpret any socket-related error as an I/O error
			raise IOError('Socket communication error')


class api:
        def __init__(self):
                self.controller=PmacEthernetInterface(self);

                #File attributes
                self.data = []
                self.column_max = 0
                self.line_max = 0

                self.wait_step = 0.01

        def connect(self, ip, port):     
                self.controller.connect(ip,port);

        def disconnect(self):
                self.controller.disconnect();

        def __SEQ_readfile(self, _column=0, _line=0):
                value = ''                    
                try:
                    value = self.data[_line][_column] #get value
                    val = float(value) #test if float (convert string to float)
                    return value #return value
                
                except IndexError: #out of tab bounds, raise Exception
                    if _column < 0 or _column > self.column_max :
                        raise Exception ('Column {} incorrect: column number must be between 1 and {} included'.format(_column, self.column_max+1))
                    elif _line < 0 or _line > self.line_max :
                        raise Exception ('Line {} incorrect: line number must be between 1 and {} included'.format(_line, self.line_max+1))

                except ValueError: #float convertion failed, raise Exception
                    raise Exception("Value is not a float")
        def term(self, cmd, log=False):                
                #if log == True:
                        #print(cmd, file=open("LOG.pmc", "a"))
                return self.controller.sendCommand(cmd, True).split()[0];            
        
        def SEQ_download(self, file_path, pause_stab, pause_mes, dec_nb):
                try:                        
                        #check if parameters are float
                        param1 = float(pause_stab)
                        param2 = float(pause_mes)
                        param3 = int(dec_nb)
                        
                        #SEQUENCE
                        #print("&2", file=open("LOG.pmc", "w"))
                        self.term("&2",False)
                        self.term("close",True)
                        self.term("delete gather",True)
                        self.term("open prog 20 clear",True)
                        self.term("P3400={}".format(pause_stab),True)
                        self.term("P3401={}".format(pause_mes),True)
                        self.term("P3403={}".format(dec_nb),True)
                        self.term("P3404=1 Q39=1 Q70=0",True)

                        #read file
                        self.data = []

                        with open(file_path, newline='') as FILE: #open file at specified path
                            #get 'each line as an array (separates each elements that were separated by space)
                            for row in reader(FILE, delimiter='\t'):
                                #add each line to the data array, creating a 2D array from file content
                                self.data.append(row)

                        #get the dimension of our grid
                        self.column_max = len(self.data[0])-1
                        self.line_max = len(self.data)-1

                        #set previous line as empty
                        lastvalue = ["NONE"]* (self.column_max+1)

                        #for each line
                        for line in range(0, self.line_max+1):

                                #start command with line number
                                cmd = "Q40={}".format(line+1)
                                #init an empty new line
                                currentvalue = []

                                #for each column
                                for column in range(0, self.column_max+1):

                                        #get current value
                                        currentvalue.append(self.__SEQ_readfile(column,line))
                                        #add value to command only if different from the value at the previous line
                                        #if currentvalue[column] != lastvalue[column]:
                                                #cmd += " Q7{}={}".format(column+1, currentvalue[column])
                                        cmd += " Q7{}={}".format(column+1, currentvalue[column])
                                        #print("output: ",cmd, line, column)
                                #save current line as the last used
                                lastvalue = currentvalue
                                #send the whole command for this line
                                self.term(cmd,True)
                                self.term("call 1010",True)
                                self.term("call 1021",True)

                        self.term("Q39=0",True)
                        self.term("P1002=1",True)
                        self.term("close",True)
                        print("Sequence downloaded successfully!")

                except ValueError as error: #incorrect paramaters
                        print("Error: parameters type not respected: {}".format(error))

                except Exception as exception: #error during reading file
                        print("Error: wrong value when reading file: {}".format(exception))
                        self.term("close",True)
