#!/usr/bin/python3

import SYM_HexaPy, os


#Parameters
ip = "192.168.16.239"
port = 1025
SEQ_file_path = "Sequence.txt"
SEQ_pause_stab = 0
SEQ_pause_mes = 0
SEQ_dec_nb = 0


#Connect to the controller
hexapod = SYM_HexaPy.api()
hexapod.connect(ip,port)
hexapod.term("&2 Q20=2",False) #STOP command
hexapod.SEQ_download(SEQ_file_path, SEQ_pause_stab, SEQ_pause_mes, SEQ_dec_nb)

#hexapod.term("&2 Q20=12",False) #START sequence
hexapod.disconnect()

exit()
