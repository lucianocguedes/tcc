#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

#geraplot.py
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image

fname = "folha_ponta@2500eV_0001_new(1).dat"
#fname = "Au40um_0002_new.dat"
a = np.loadtxt(fname, skiprows=1, usecols=range(9))
print(a.shape)

print("Chose the RGB channels for the image, according to the collumns of the file")
red_col=input("column for red: ")
green_col=input("column for green: ")
blue_col=input("column for blue: ")  
symetriexz=a[:,2:3]
symetriexy=a[:,3:4]
pixelZ=a[:,0:1]
pixelY=a[:,1:2]
maxZ=(np.amax(pixelZ)+1)
maxY=(np.amax(pixelY)+1)
red=a[:,int(red_col):(int(red_col)+1)].reshape(int(maxZ),int(maxY))
#blue=np.zeros([int(maxZ), int(maxY)])
blue=a[:,int(blue_col):(int(blue_col)+1)].reshape(int(maxZ),int(maxY))
green=a[:,int(green_col):(int(green_col)+1)].reshape(int(maxZ),int(maxY))
maxred=np.amax(red)
maxgreen=np.amax(green)
maxblue=np.amax(blue)
maxtotal=(maxgreen+maxblue+maxred)/3
print(red.shape)


#RGB = np.zeros([101, 101, 3], dtype=np.uint8)
RGB = np.zeros([int(maxZ), int(maxY), 3], dtype=np.uint8)            
RGB[:,:,2] =255*(blue)/(maxtotal) 
RGB[:,:,1] =255*(green)/(maxtotal)
RGB[:,:,0] =255*(red)/(maxtotal)
plt.hist(red.flatten(), 256, range=(0.0,maxred), fc='white', ec='red')
plt.hist(green.flatten(), 256, range=(0.0,maxgreen), fc='white', ec='green')
plt.hist(blue.flatten(), 256, range=(0.0,maxblue), fc='white', ec='blue')
plt.title("Histogram of red pixels")
plt.show(1)
plt.imshow(RGB)
plt.title("Imagem varredura 2D,[RGB]")
plt.show(2)
plt.imsave('ibagem.png',RGB)
