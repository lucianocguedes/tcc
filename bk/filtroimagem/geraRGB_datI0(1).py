#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

#geraplot.py
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image
from scipy.fftpack import fft

os.system('rm -f tabela.dat')
os.system('rm -f linha.dat')
os.system('rm -f y.dat')
os.system('rm -f z.dat')

##SELECT DATA PIXELS
#Au40um25pts-20um_0001.dat Au40um25pts-20um_30graus_0001.dat
#Au40um160pts-5um_30graus_0001.dat
#Au40um7000pts-5um_30graus_0001.dat
#Au40um10000pts-5um_0001.dat 
os.system('awk "FNR>=8" Au40um7000pts-5um_30graus_0001.dat>> tabela.dat')
fname1 = "tabela.dat"
a = np.loadtxt(fname1, usecols=range(6))
symetriexz=a[:,2:3]
symetriexy=a[:,3:4]

##FIND THE STEP NUMBERS
os.system('awk "FNR==5" Au40um7000pts-5um_30graus_0001.dat|sed "s/#S 1 mesh(SYMETRIEZ, //"|sed "s/, SYMETRIEY, //"|tr "]" "\n"|sed "s/\[//">> linha.dat')
os.system('awk "FNR==1" linha.dat>> z.dat')
os.system('awk "FNR==2" linha.dat>> y.dat')
fname2 = "z.dat"
fname3 = "y.dat"
b1=np.genfromtxt(fname2, delimiter=", ")
b2=np.genfromtxt(fname3, delimiter=", ")
lz=b1.size
ly=b2.size
print(ly)
##Input information
print("Chose the RGB channels for the image, according to the collumns of the file")
red_col=input("column for red: ")
green_col=input("column for green: ")
blue_col=input("column for blue: ")  



zlines=int(lz)#Z
ylines=int(ly)#Y
red=a[:,int(red_col):(int(red_col)+1)].reshape(zlines,ylines)
blue=a[:,int(blue_col):(int(blue_col)+1)].reshape(zlines,ylines)
green=a[:,int(green_col):(int(green_col)+1)].reshape(zlines,ylines)
current=a[:,2:3].reshape(zlines,ylines)
maxcurrent=np.amax(current)
mincurrent=np.amin(current)
maxtotal=np.amax(a[:,4:9])
maxred=np.amax(red)
maxgreen=np.amax(green)
maxblue=np.amax(blue)
#print(red.shape)

RGB = np.zeros([zlines, ylines, 3], dtype=np.uint8)             
RGB[:,:,2] =255*((blue)/(maxtotal))*(2-((current)/(maxcurrent)))/3
RGB[:,:,1] =255*(green)/(maxtotal)*(2-((current)/(maxcurrent)))/3
RGB[:,:,0] =255*((red)/(maxtotal))*(2-((current)/(maxcurrent)))/3
plt.hist(red.flatten(), 256, range=(0.0,maxred), fc='white', ec='red')
plt.hist(green.flatten(), 256, range=(0.0,maxgreen), fc='white', ec='green')
plt.hist(blue.flatten(), 256, range=(0.0,maxblue), fc='white', ec='blue')
plt.title("Histogram of red pixels")
plt.show(1)

plt.imshow(RGB)
plt.title("Imagem varredura 2D,[RGB]")
plt.show(2)
plt.imsave('Au40um7000pts-5um_30graus_0001.png',RGB)
plt.imshow(current)
plt.show(3)
plt.imshow((3-((current-mincurrent)/(maxcurrent-mincurrent))))
plt.show(4)
mediag=np.zeros([2,zlines])
mediar=np.zeros([2,zlines])
i=0
k=0
for i in range(0,zlines) :
    mediag[0,i]=5*i
    mediar[0,i]=5*i
    mediag[1,i]=(((green[i,:].mean())/maxgreen)*(2-((current[i,:].mean())/(maxcurrent)))/3)
    mediar[1,i]=(((red[i,:].mean())/maxred)*(2-((current[i,:].mean())/(maxcurrent)))/3)
casa=np.zeros([zlines])
casa[:]=mediar[:,1].mean()
print(casa)
plt.plot(mediag[0,:],mediag[1,:],'-go')
plt.plot(mediar[0,:],mediar[1,:],'-ro')
plt.show(5)
os.system('rm -f tabela.dat')
os.system('rm -f linha.dat')
os.system('rm -f y.dat')
os.system('rm -f z.dat')
