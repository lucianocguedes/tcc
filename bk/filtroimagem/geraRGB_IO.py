#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

#geraplot.py
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image

fname = "mesh_2mmx2mm-25um_0002.mca"
a = np.loadtxt(fname, skiprows=1, usecols=range(7))
print(a.shape)

print("Chose the RGB channels for the image, according to the collumns of the file")
red_col=input("column for red: ")
green_col=input("column for green: ")
blue_col=input("column for blue: ")  
pixelZ=a[:,0:1]
pixelY=a[:,1:2]

print(pixelZ.shape)
maxZ=(np.amax(pixelZ)+1)
maxY=(np.amax(pixelY)+1)
print(maxZ)
print(maxY)

red=a[:,int(red_col):(int(red_col)+1)].reshape(int(maxZ),int(maxY))
blue=a[:,int(blue_col):(int(blue_col)+1)].reshape(int(maxZ),int(maxY))
green=a[:,int(green_col):(int(green_col)+1)].reshape(int(maxZ),int(maxY))

maxred=np.amax(red)
maxgreen=np.amax(green)
maxblue=np.amax(blue)

current=a[:,4:5].reshape(int(maxZ),int(maxY))
maxcu=np.amax(current)
maxtotal=(maxred+maxgreen+maxblue)/3
print(current.shape)

RGB = np.zeros([int(maxZ), int(maxY), 3], dtype=np.uint8)             
RGB[:,:,2] =255*(blue)/(maxtotal)*(current/maxcu)
RGB[:,:,1] =255*(green)/(maxtotal)*(current/maxcu)
RGB[:,:,0] =255*(red)/(maxtotal)*(current/maxcu)

plt.hist(red.flatten(), 256, range=(0.0,maxred), fc='white', ec='red')
plt.hist(green.flatten(), 256, range=(0.0,maxgreen), fc='white', ec='green')
plt.hist(blue.flatten(), 256, range=(0.0,maxblue), fc='white', ec='blue')
plt.title("Histogram of red pixels")
plt.show(1)
plt.imshow(RGB)
plt.title("Imagem varredura 2D,[RGB]")
plt.show(2)
plt.imsave('ibagem.png',RGB)
plt.imshow(current)
plt.show(3)
