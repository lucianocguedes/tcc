#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

#analise_hexa.py
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image
from matplotlib import cm
from matplotlib import colors
from mpl_toolkits.mplot3d import Axes3D
from scipy import signal as sg
import pylab as pl
asize = 2
sigma_x = 1.
sigma_y = 1.
x=0
y=0
x0=1500
y0=-1500
xf=1000
yf=-2000
hx=-20
hy=-30
nx=100
ny=100

px=10
py=10

'''
RGB =  np.random.rand(nx,ny,3)
RGB_novo = np.zeros([nx*10, ny*10, 3], dtype=np.uint8) 
'''
fname = "abc"
fname5 = "malha_grande2.mca"
a = np.loadtxt(fname, skiprows=1 ,usecols=range(10))
###

print("Chose the RGB channels for the image, according to the collumns of the file")
red_col=input("column for red(5 a 9): ")
green_col=input("column for green: ")
blue_col=input("column for blue: ")  
pixelZ=a[:,0:1]
pixelY=a[:,1:2]

print(pixelZ.shape)
maxZ=(np.amax(pixelZ)+1)
maxZ=5
maxY=(np.amax(pixelY)+1)
maxZ=46
print(maxZ)
print(maxY)
red=np.zeros([276,1])
print(red.shape)
red[0:276,:]=a[:,int(red_col):(int(red_col)+1)]
red=red.reshape(int(maxZ),int(maxY))
blue[0:276,:]=a[:,int(blue_col):(int(blue_col)+1)]
blue=blue.reshape(int(maxZ),int(maxY))
green[0:276,:]=a[:,int(green_col):(int(green_col)+1)]
green=green.reshape(int(maxZ),int(maxY))
blue=np.zeros([276,1]).reshape(int(maxZ),int(maxY))
green=np.zeros([276,1]).reshape(int(maxZ),int(maxY))
#blue=a[:,int(blue_col):(int(blue_col)+1)].reshape(int(maxZ),int(maxY))
#green=a[:,int(green_col):(int(green_col)+1)].reshape(int(maxZ),int(maxY))

maxred=np.amax(red)
maxgreen=np.amax(green)
maxblue=np.amax(blue)
'''
#current=a[:,4:5].reshape(int(maxZ),int(maxY))
maxcu=np.amax(current)
maxtotal=(maxred+maxgreen+maxblue)/3
print(current.shape)
'''
RGB = np.zeros([int(maxZ), int(maxY), 3], dtype=np.uint8)             
RGB[:,:,2] =255*(blue)
RGB[:,:,1] =255*(green)
RGB[:,:,0] =255*(red)
'''nx=int(maxY)
ny=int(maxZ)
RGB_novo = np.zeros([nx*px, ny*py, 3], dtype=np.uint8) 
correcao = np.zeros([nx*px, ny*py, 3], dtype=np.uint8) 
correcao[:,:,:]=255
####

RGB_novo[1:10,1:10,1]=200*RGB[1,1,1]
for i in range (0,nx):
	for j in range (0,ny):
		RGB_novo[(px*i):(px*i+10),(py*j):(py*j+10),:]=255*RGB[i,j,:]
Ax=0
Bx=0
Cx=0
Ay=0
By=0
Cy=0
for i in range (0,(nx-1)):
	for j in range (0,(ny-1)):

		#Ax=(RGB_novo[(px*i+9):(px*i+10),:,:]+RGB_novo[(px*i+10):(px*i+11),:,:])/2

		#RGB_novo[(px*i+9):(px*i+11),:,:]=Ax
		Ax=0.7*(RGB_novo[(px*i+7):(px*i+8),:,:])+0.3*RGB_novo[(px*i+10):(px*i+11),:,:]


		Ay=0.7*RGB_novo[:,(py*j+7):(py*j+8),:]+0.3*RGB_novo[:,(py*j+10):(py*j+11),:]

		Bx=0.5*RGB_novo[(px*i+8):(px*i+9),:,:]+0.5*RGB_novo[(px*i+11):(px*i+12),:,:]


		By=0.5*RGB_novo[:,(py*j+8):(py*j+9),:]+0.5*RGB_novo[:,(py*j+11):(py*j+12),:]

		Cx=0.3*RGB_novo[(px*i+9):(px*i+10),:,:]+0.7*RGB_novo[(px*i+12):(px*i+13),:,:]


		Cy=0.3*RGB_novo[:,(py*j+9):(py*j+10),:]+0.7*RGB_novo[:,(py*j+12):(py*j+13),:]
		
		RGB_novo[(px*i+7):(px*i+9),:,:]=Ax
		RGB_novo[:,(py*j+7):(py*j+9),:]=Ay
		RGB_novo[(px*i+9):(px*i+11),:,:]=Bx
		RGB_novo[:,(py*j+9):(py*j+11),:]=By
		RGB_novo[(px*i+11):(px*i+13),:,:]=Bx
		RGB_novo[:,(py*j+11):(py*j+13),:]=By

#RGB_novo[9:11,:,:]=(RGB_novo[9:10,:,:]+RGB_novo[10:11,:,:])/2
#RGB_novo[:,9:11,:]=(RGB_novo[:,9:10,:]+RGB_novo[:,10:11,:])/2'''
plt.imshow(RGB)
plt.show(1)
plt.imshow(correcao-RGB_novo)
plt.show(2)
