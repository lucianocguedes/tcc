import numpy as np
import os
import matplotlib.pyplot as plt

fname=input("file name: ")

a = np.loadtxt(fname, skiprows=1, usecols=range(15))

index_w=a[:,0]
index_k=a[:,1]
index_j=a[:,2]
pos_z=a[:,4]
pos_x=a[:,5]
I0=a[:,6]
S1=a[:,3]
S2=a[:,7]
S3=a[:,8]
S4=a[:,9]
S5=a[:,10]
S6=a[:,11]
S7=a[:,12]
S8=a[:,13]
time=a[:,14]
#t_max=np.amax(time)
#I0_max=np.amax(I0)
#t_max=2.6*(10**(-6))
#I0_max=3.4*(10**(-10))
#I0=(I0*1.04*t_max/I0_max)
#time=time
print("the min and max value of;\nw_min= ",np.amin(index_w),'\t',"w_max= ",np.amax(index_w))
print("k_min= ",np.amin(index_k),'\t',"k_max= ",np.amax(index_k))
print("j_min= ",np.amin(index_j),'\t',"j_max= ",np.amax(index_j))
print("posx_min= ",np.amin(pos_x),'\t',"posx_max= ",np.amax(pos_x))
print("posz_min= ",np.amin(pos_z),'\t',"posz_max= ",np.amax(pos_z))
print("I0_min= ",np.amin(I0),'\t',"I0_max= ",np.amax(I0))
print("S1_min= ",np.amin(S1),'\t',"S1_max= ",np.amax(S1))
print("S2_min= ",np.amin(S2),'\t',"S2_max= ",np.amax(S2))
print("S3_min= ",np.amin(S3),'\t',"S3_max= ",np.amax(S3))
print("S4_min= ",np.amin(S4),'\t',"S4_max= ",np.amax(S4))
print("S5_min= ",np.amin(S5),'\t',"S5_max= ",np.amax(S5))

print("S6_min= ",np.amin(S6),'\t',"S6_max= ",np.amax(S6))
print("S7_min= ",np.amin(S7),'\t',"S7_max= ",np.amax(S7))
print("S8_min= ",np.amin(S8),'\t',"S8_max= ",np.amax(S8))
print("trans_min= ",np.amin(time),'\t',"trans_max= ",np.amax(time))

total=np.column_stack((index_k,index_j,S3,S4,time))

datafile_path=input("save as: ")

for i in range(0,60):
    with open(datafile_path+'_'+str(i), 'w') as f:
        f.write('row\tcolumn\tCl\tK\ttrans\n')
        a=total[(0+i*46*6):(46*6*(i+1))]
        np.savetxt(f, a, fmt='%s',delimiter='\t')

