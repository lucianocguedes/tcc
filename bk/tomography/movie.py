import numpy as np
import moviepy.editor as mpy

def make_frame(t):

    h = 6
    w = 46
    a = np.loadtxt("wer",skiprows=1, usecols=range(15))
    ari = np.zeros((h*2, w*2, 3))
    m=int(t/0.1)
    cor1=a[(0+m*46*6):(46*6*(m+1)),14]/a[(0+m*46*6):(46*6*(m+1)),6]
    cor2=a[(0+m*46*6):(46*6*(m+1)),8]/a[(0+m*46*6):(46*6*(m+1)),6]
    cor3=a[(0+m*46*6):(46*6*(m+1)),9]/a[(0+m*46*6):(46*6*(m+1)),6]
    ar = np.zeros((6, 46, 3))
    ar[:,:,2]=(cor1.reshape((6,46)))*255/np.amax(cor1)
    ar[:,:,1]=(cor2.reshape((6,46)))*255/np.amax(cor2)
    ar[:,:,0]=(cor3.reshape((6,46)))*255/np.amax(cor3)
    print(t)
    for hi in range(h):
        for wi in range(w):
            for ci in range(3):
                ari[(2*hi+0):(2+2*hi), (0+wi*2):(2+wi*2), 0] = ar[hi, wi, 0]
                ari[(2*hi+0):(2+2*hi), (0+wi*2):(2+wi*2), 1] = ar[hi, wi, 1]
                ari[(2*hi+0):(2+2*hi), (0+wi*2):(2+wi*2), 2] = ar[hi, wi, 2]

    return ari


if __name__ == '__main__':

    clip = mpy.VideoClip(make_frame, duration=6)
    #clip.write_videofile('banana2.mp4',audio=False,fps=10)
    clip.write_gif('ani.gif', fps=10)

