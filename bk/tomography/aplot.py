import numpy as np
import pyqtgraph as pg


a = np.loadtxt("wer",skiprows=1, usecols=range(15))
m=int(input("which slice?"))
cor1=a[(0+m*46*6):(46*6*(m+1)),14]/a[(0+m*46*6):(46*6*(m+1)),6]
cor2=a[(0+m*46*6):(46*6*(m+1)),8]/a[(0+m*46*6):(46*6*(m+1)),6]
cor3=a[(0+m*46*6):(46*6*(m+1)),9]/a[(0+m*46*6):(46*6*(m+1)),6]
data = np.zeros((6, 46, 3))
data[:,:,2]=(cor1.reshape((6,46)))/np.amax(cor1)
data[:,:,0]=(cor2.reshape((6,46)))/np.amax(cor2)
data[:,:,1]=(cor3.reshape((6,46)))/np.amax(cor3)

pg.image(data)



if __name__ == '__main__':
    import sys
    if sys.flags.interactive != 1 or not hasattr(QtCore, 'PYQT_VERSION'):
        pg.QtGui.QApplication.exec_()

