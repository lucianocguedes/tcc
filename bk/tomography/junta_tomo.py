#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

#geraplot.py
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image

#fname =input("insert file name: ")


fname_1 = "teste.dat"
a1 = np.loadtxt(fname_1, skiprows=14, usecols=range(15))
fname_2 = "teste_parte2"
a2 = np.loadtxt(fname_2, skiprows=0, usecols=range(15))
fname_3 = "teste_parte3"
a3 = np.loadtxt(fname_3, skiprows=0, usecols=range(15))

datafile_path=input("name to save: ")

part1=a1[np.lexsort((a1[:,2], a1[:,1],a1[:,0]))]
part2=a2[np.lexsort((a2[:,2], a2[:,1],a2[:,0]))]
part3=a3[np.lexsort((a3[:,2], a3[:,1],a3[:,0]))]

with open(datafile_path, 'w') as datafile_id:
    datafile_id.write('index_w\tindex_k\tindex_j\ttheta\thexaz\thexay\tI0\tS1\tS2\tS3\tS4\tS5\tS6\tS7\tS8\ttime\n')
    np.savetxt(datafile_id, part1, fmt='%s',delimiter='\t')
    np.savetxt(datafile_id, part2, fmt='%s',delimiter='\t')
    np.savetxt(datafile_id, part3, fmt='%s',delimiter='\t')

a = np.loadtxt(datafile_path, skiprows=1, usecols=range(15))
new=a[np.lexsort((a[:,2], a[:,1],a[:,0]))]
n=new[:,1].size
print(n)

x=np.linspace(0, n, n)

plt.plot(x,new[:,1])
plt.plot(x,new[:,2])
plt.plot(x,new[:,0])

with open(datafile_path, 'w') as datafile_id:
    datafile_id.write('index_w\tindex_k\tindex_j\ttheta\thexaz\thexay\tI0\tS1\tS2\tS3\tS4\tS5\tS6\tS7\tS8\ttime\n')
    np.savetxt(datafile_id, new, fmt='%s',delimiter='\t')
plt.show()

