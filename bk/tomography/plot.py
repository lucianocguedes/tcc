import numpy as np
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

# Interpret image data as row-major instead of col-major
pg.setConfigOptions(imageAxisOrder='row-major')

app = QtGui.QApplication([])

## Create window with ImageView widget
win = QtGui.QMainWindow()
win.resize(800,800)
imv = pg.ImageView()
win.setCentralWidget(imv)
win.show()
win.setWindowTitle('pyqtgraph example: ImageView')

## Display the data and assign each frame a time value from 1.0 to 3.0
a = np.loadtxt("wer",skiprows=1, usecols=range(15))
casa=np.linspace(0, 60*46*6, num=60*46*6)
casa3=a[:,14]/a[:,6]
casa3=casa3.reshape((60,6,46))
print(casa3.shape)
imv.setImage(casa3, xvals=np.linspace(0, 60, casa3.shape[0]))
cor1=a[0:46,14]/a[0:46,6]
cor2=a[0:46,8]/a[0:46,6]
cor3=a[0:46,9]/a[0:46,6]

## Set a custom color map
colors = [
        (255, 255, 255),
    (208, 171, 141),
    (150, 87, 60),
    (84, 42, 55),
    (45, 5, 61),
    (0, 0, 0)
]
#pg.image([cor1,cor2,cor3], levels=(0, 255))
#cmap = pg.ColorMap(pos=np.linspace(0.0, 1.0, 6), color=colors)
#imv.setColorMap(cmap)

## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

