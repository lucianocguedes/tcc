#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

#geraplot.py
import numpy as np
import os
import matplotlib.pyplot as plt

fname = "folha_ponta@2500eV_0001_new(1).dat"
a = np.loadtxt(fname, skiprows=1, usecols=range(9))
print(a.shape)
columX_max=np.amax(a[:,0:1])
columY_max=np.amax(a[:,1:2])
columX_min=np.amin(a[:,0:1])
columY_min=np.amin(a[:,1:2])
columY=a[:,1:2]
symetriexz=a[:,2:3]
symetriexy=a[:,3:4]
Og=a[:,4:5]
Al=a[:,5:6]
Si=a[:,6:7]
Ph=a[:,7:8]
Sf=a[:,8:9]
print(Sf.shape)
u = np.linspace(0,100,101)
v= np.linspace(0,100,101)
z= np.linspace(0,100,10201)
h=Si.reshape(101,101)
print(u,v)
X,Y = np.meshgrid(u, v)
#data = np.random.random(size=Si)
plt.set_cmap('hot')
plt.pcolor(X,Y,h)
#plt.imshow([(Si,Al,Og)])
plt.show()
