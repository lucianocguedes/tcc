#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

#geraplot.py
import numpy as np
import os
import matplotlib.pyplot as plt
from PIL import Image

os.system('rm -f tabela.dat')
os.system('rm -f linha.dat')
os.system('rm -f y.dat')
os.system('rm -f z.dat')

#teste-Au40um900pts_0001.dat
#Au40um1200pts-40um_0001.dat
#Au40um10000pts-5um_0001.dat

##SELECT DATA PIXELS
os.system('awk "FNR>=8" Au40um10000pts-5um_0001.dat >> tabela.dat')
fname1 = "tabela.dat"
os.system('wc -l tabela.dat')
os.system('cat tabela.dat')
a = np.loadtxt(fname1, usecols=range(6))
symetriexz=a[:,1:2]
symetriexy=a[:,2:3]
current=a[:,2:3]

##FIND THE STEP NUMBERS
os.system('awk "FNR==5" Au40um10000pts-5um_0001.dat|sed "s/#S 1 mesh(SYMETRIEZ, //"|sed "s/, SYMETRIEY, //"|tr "]" "\n"|sed "s/\[//">> linha.dat')
os.system('awk "FNR==1" linha.dat>> z.dat')
os.system('awk "FNR==2" linha.dat>> y.dat')
fname2 = "z.dat"
fname3 = "y.dat"
b1=np.genfromtxt(fname2, delimiter=", ")
b2=np.genfromtxt(fname3, delimiter=", ")
lz=b1.size
ly=b2.size
print(ly)

##Input information
print("Chose the RGB channels for the image, according to the collumns of the file")
red_col=input("column for red: ")
green_col=input("column for green: ")
blue_col=input("column for blue: ")  



zlines=int(lz)#Z
ylines=int(ly)#Y
red = np.zeros([zlines, ylines])
blue = np.zeros([zlines, ylines])
green = np.zeros([zlines, ylines])
red[0:84,:]=a[0:8484,int(red_col):(int(red_col)+1)].reshape(84,ylines)
blue[0:84,:]=a[0:8484,int(blue_col):(int(blue_col)+1)].reshape(84,ylines)
green[0:84,:]=a[0:8484,int(green_col):(int(green_col)+1)].reshape(84,ylines)

maxtotal=np.amax(a[:,3:5])
maxred=np.amax(red)
maxgreen=np.amax(green)
maxblue=np.amax(blue)
#print(red.shape)

RGB = np.zeros([zlines, ylines, 3], dtype=np.uint8)             
RGB[:,:,2] =255*(blue)/(maxtotal) 
RGB[:,:,1] =255*(green)/(maxtotal)
RGB[:,:,0] =255*(red)/(maxtotal)

plt.hist(red.flatten(), 256, range=(0.0,maxred), fc='white', ec='red')
plt.hist(green.flatten(), 256, range=(0.0,maxgreen), fc='white', ec='green')
plt.hist(blue.flatten(), 256, range=(0.0,maxblue), fc='white', ec='blue')
plt.title("Histogram of red pixels")
plt.show(1)

plt.imshow(RGB)
plt.title("Imagem varredura 2D,[RGB]")
plt.show(2)
plt.imsave('Au40um10000pts-5um_0001.png',RGB)


os.system('rm -f tabela.dat')
os.system('rm -f linha.dat')
os.system('rm -f y.dat')
os.system('rm -f z.dat')
