#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

# hello3_app_with_class.py

from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QToolTip, QPushButton, QApplication
from PyQt5 import uic, QtGui
from PyQt5.QtGui import QFont

import sys
'''
class Window(QMainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        uic.loadUi('hello.ui', baseinstance=self)
        self.show()
	# Bind button to method
        self.pushButton_action.clicked.connect(self.myAction)#

def myAction(self):
        awesome_text = self.lineEdit.text() # Text form LineEdit
        # Define a message box
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("Information")
        msg.setText('The awesome text is: ' + awesome_text)
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        # Show message
        reply = msg.exec_()
        # Get pushed button
        if reply == msg.Ok: print('"Ok" button was pushed.')
        elif reply == msg.Cancel: print('"Cancel" button was pushed.')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec_())
'''   


class Example(QWidget):
    
    def __init__(self):
        super().__init__()
        
        self.initUI()
        
        
    def initUI(self):
        
        QToolTip.setFont(QFont('SansSerif', 12)) #somente para setar o tmano hda fonte da caixa de mensagem      
        btn = QPushButton('Olhe aqui', self)
	btn.clicked.connect(QApplication.instance().quit)
        btn.setToolTip('Esse botao eh um <b>QPushButton</b>')
        btn.resize(btn.sizeHint())
        btn.move(50, 50)#(xpostion,yposition)
     
        
        self.setGeometry(300, 300, 200, 200)#(xpos_tela,ypos_tela,xsize,ysize)
        self.setWindowTitle('Janelinha')    
        self.show()
        
        
if __name__ == '__main__':
    
	app = QApplication(sys.argv)
	a=Example()
	sys.exit(app.exec_())
