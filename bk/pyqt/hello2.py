# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'hello.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(518, 427)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.label_3 = QtWidgets.QLabel(Form)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.y0_button = QtWidgets.QDoubleSpinBox(Form)
        self.y0_button.setObjectName("y0_button")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.y0_button)
        self.label_4 = QtWidgets.QLabel(Form)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.z0_button = QtWidgets.QDoubleSpinBox(Form)
        self.z0_button.setObjectName("z0_button")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.z0_button)
        self.label_2 = QtWidgets.QLabel(Form)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.x_button = QtWidgets.QDoubleSpinBox(Form)
        self.x_button.setObjectName("x_button")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.x_button)
        self.gridLayout.addLayout(self.formLayout, 3, 2, 1, 1)
        self.label = QtWidgets.QLabel(Form)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 1, 1, 1)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.clear_button = QtWidgets.QPushButton(Form)
        self.clear_button.setObjectName("clear_button")
        self.horizontalLayout.addWidget(self.clear_button)
        self.gridLayout.addLayout(self.horizontalLayout, 7, 2, 2, 1)
        self.formLayout_4 = QtWidgets.QFormLayout()
        self.formLayout_4.setObjectName("formLayout_4")
        self.label_6 = QtWidgets.QLabel(Form)
        self.label_6.setObjectName("label_6")
        self.formLayout_4.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_6)
        self.yf_button = QtWidgets.QDoubleSpinBox(Form)
        self.yf_button.setObjectName("yf_button")
        self.formLayout_4.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.yf_button)
        self.label_7 = QtWidgets.QLabel(Form)
        self.label_7.setObjectName("label_7")
        self.formLayout_4.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_7)
        self.zf_button = QtWidgets.QDoubleSpinBox(Form)
        self.zf_button.setObjectName("zf_button")
        self.formLayout_4.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.zf_button)
        self.gridLayout.addLayout(self.formLayout_4, 3, 3, 1, 1)
        self.close_button = QtWidgets.QPushButton(Form)
        self.close_button.setObjectName("close_button")
        self.gridLayout.addWidget(self.close_button, 7, 3, 1, 1)
        self.action_button = QtWidgets.QPushButton(Form)
        self.action_button.setObjectName("action_button")
        self.gridLayout.addWidget(self.action_button, 4, 3, 1, 1)

        self.retranslateUi(Form)
        self.close_button.clicked.connect(Form.close)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Hello PyQt"))
        self.label_3.setText(_translate("Form", "y0"))
        self.label_4.setText(_translate("Form", "z0"))
        self.label_2.setText(_translate("Form", "x"))
        self.label.setText(_translate("Form", "GERA_MESH"))
        self.clear_button.setText(_translate("Form", "Clear"))
        self.label_6.setText(_translate("Form", "yf"))
        self.label_7.setText(_translate("Form", "zf"))
        self.close_button.setText(_translate("Form", "Close"))
        self.action_button.setText(_translate("Form", "Action"))

