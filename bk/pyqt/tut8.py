#!/home/ABTLUS/luciano.guedes/anaconda3/bin/python

# hello3_app_with_class.py

# hello3_app_with_control.py

from PyQt5.QtWidgets import QApplication, QMainWindow, QMessageBox
from PyQt5.QtCore import QTimer, QTime
from PyQt5 import uic
import sys


class Window(QMainWindow):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)
        uic.loadUi('hello.ui', baseinstance=self)
        self.show()
        #Timer
        #self.timer=QTimer()
        #self.timer.timeout.connect(self._update)
        #self.timer.start(1000)

        # Bind button to method
        self.action_b.clicked.connect(self.myAction)
        self.import_b.clicked.connect(self.import_paramters)
        self.start_move.clicked.connect(self._update)
    def _update(self):
        #Update display each one second
        time=QTime.currentTime().toString()
        pos_y=self.Y_form.text()
        self.lcd.display(time)
        self.lcd_y.display(pos_y)

    def mandavalor(self):
        global a
        a+=1

    def import_paramters(self):
        import params
        x ="0"  # Text form LineEdit
        Rx="0"
        Ry="0"
        Rz="0"
        y_0 = str(params.y0)
        z_0 = str(params.z0)
        y_f = str(params.yf)
        z_f = str(params.zf)
        ny_steps = str(params.ny)
        nz_steps = str(params.nz)
        tstab = str(params.tstab)
        tmes = str(params.tmes)
        self.x.setText(x) # Text form LineEdit
        self.y0.setText(Rx)
        self.z0.setText(Ry)
        self.yf.setText(Rz)
        self.zf.setText(z_f)
        self.Rx.setText(Rx)
        self.Ry.setText(Ry)
        self.Rz.setText(Rz)
        self.ny.setText(ny_steps)
        self.nz.setText(nz_steps)
        self.tstab.setText(tstab)
        self.tmes.setText(tmes)

    def myAction(self):
        x_pos = self.x.text() # Text form LineEdit
        y_0 = self.y0.text()
        z_0 = self.z0.text()
        y_f = self.yf.text()
        z_f = self.zf.text()
        Rx_pos = self.Rx.text()
        Ry_pos = self.Ry.text()
        Rz_pos = self.Rz.text()
        ny_steps = self.ny.text()
        #self.ny.text()="a"
        nz_steps = self.nz.text()
        tstab = self.tstab.text()
        tmes = self.tmes.text()

        print('Y vai de ',y_0,' a ',y_f,'\n','Z vai de ',z_0,' a ',z_f,'\n','X:',x_pos,' Rx:',Rx_pos,' Ry:',Ry_pos,' Rz:',Rz_pos,'\n',' t_stab:',tstab,' t_mes: ',tmes,'\n')

        # Define a message box
       

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Window()
    sys.exit(app.exec_())
