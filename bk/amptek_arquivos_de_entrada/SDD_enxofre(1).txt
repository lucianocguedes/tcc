[DP5 Configuration File]
;
; Rules:	No whitespace before command - it must start at left column 
; 		Command must end with semicolon
;		Anything after semicolon is ignored (i.e. comments)
;
;  The commands are documented on Table 4, ASCII Command Summary, on pages 66-68 of the DP5 Programmer's Gude
;
BOOT=ON;
;
;  Most important parameters.  The sequence of these parameters is important.
;
RESC=YES;		Clear the existing configuration
CLCK=AUTO;		FPGA clock: 20, 80, or AUTO
TPEA=1.0US;		Peaking time in uS
TFLA=0.2US;		Flat top in uS
TPFA=400NS;		Fast channel Peaking Time in ns:[####] (50, 100 or 400 @ 80MHz, 200, 400 or 1600 @ 20MHz)
CUSP=0%;		Cusp parameter [OFF|##] (-99 to 99%, 0% or OFF=normal trapezoid, +%=cusp-like, -%=Gaussian-like)
RESL=204US;		Reset lockout period in uS
PDMD=NORM;		Peak detect mode; NORM, MIN (for min+max)
THSL=3.125%;		Slow threshold
THFA=21;		Fast threshold (0-255)
MCAC=1024;		MCA channels: [256|512|1024|2048|4096|8192]
SOFF=OFF;		Spectrum offset: +/-####.### (in channels)
DACO=SHAPED;		DAC output selection: [#|OFF|FAST|SHAPED|INPUT|PEAK|SDD|SUM|PZBD|RTDP] (1-8)
DACF=50MV;		DAC offset; +/-### (millivolts)
PURE=ON;		Pileup reject enable: [ON|OFF]
GAIN=32.988;		Total gain; ###.###
AINP=POS;		Analog input polarity: [POS|NEG] (SDD=+, SiPIN=-)
INOF=DEF;		Input offset: [{+|-}[####]|AUTO|DEF] ('DEF' uses standard default settings; 'AUTO' searches for an appropriate setting; '####' is in mV.)
;
;  Risetime Discriminator Parameters
;
RTDE=OFF;		RTD enable; [ON|OFF]
RTDS=100%;		RTD sensitivity threshold; ###%
RTDT=10%;		RTD threshold; ##.###% (% of full-scale in spectrum - RTD only processes events above this threshold)
;
;  Baseline restorer parameters
;
BLRM=1;			BLR mode; [OFF|1]
BLRD=2;			BLR down correction; # (0-3 for BLR mode 1)
BLRU=2;			BLR up correction; # (0-3 for BLR mode 1)
;
;  MCA Preset parameters
;
PRET=60;		Preset Time (in seconds, 100mS precision): [#######.#|OFF] (0 same as OFF)
;PRER=10.123;		Preset Real Time (in seconds, 1mS precision): [#######.###|OFF] (0 same as OFF)
;PREC=10000;		Preset Counts: [##########|OFF] (up to 10 digits)
;PRCL=536;
;PRCH=538;
;
;  Power supply parameters
;
HVSE=-110;		PC5 HV setting (if PC5 present): [{+|-}####|OFF] (HV is only turned on if PC5 is proper HV polarity. SDD requires negative HV, SiPIN requires positive HV)
TECS=220;		PC5 TEC cooler setting: [###|OFF] (degrees K)
PAPS=5V;		PC5 Preamp power supply: [8.5|5|OFF|ON] ('8.5' or '5' turn on the PC5 preamp supplies if they're the proper voltage. 'ON' doesn't check.)
;
;SCOG=4;
SCOE=FALLING;		Scope trigger edge: [RI{SING}|FA{LLING}]
SCOT=12%;		Scope trigger position: [87|50|12|-25] in %
;
;  MCA parameters
;
MCAS=NORM;		MCA source: [NORM|MCS|FAST|PUR|RTD]
MCAE=ON;		Initial state of MCA enable: [ON|OF{F}]
MCSL=536;
MCSH=538;
MCST=.05S;		MCS timebase: [###.###] (in seconds, multiples of 10mS)
;
; Auxiliary I/O parameters
;
AUO1=SCA8;		AUX_OUT1 selection: [#|ICR|PILEUP}MCSTB|ONESH|DETRES|MCAEN|PEAKH|SCA8]
AUO2=ICR;		AUX_OUT2 selection: [#|ICR|DIAG|PEAKH|ONESH|RTDOS|RTDREJ|LIVE|VETO]
TPMO=OFF;		Test pulser mode (output DAC): [OFF|+SNG|+DBL|-SNG|-DBL] (single or double pulses, + or -)
GPED=RI;		General purpose (GP) counter edge: [RI{SING}|FA{LLING}]
GPIN=DETRES;		GP counter input: [#|AUX1|AUX2|PILEUP|RTDREJ|SCA8|TBD|DETRES|OFF]
GPME=ON;		GP counter MCA EN: [ON|OF{F}] ('ON' means GP counter is disabled when MCA is disabled)
GPGA=ON;		GP counter Gate EN: [ON|OF{F}] ('ON' means GP counter is conditioned by GATE)
GPMC=ON;		GP clear control: [ON|OF{F}] ('ON' means GP counter is cleared when MCA is cleared; otherwise, command clears it)
;
; SCA Parameters
;   For each SCA, first set the index, then then subsequent commands apply until new index.
;   If no thresholds are set, then by default thresholds range from THSL to MCAC, ie from slow threshold to max of spectrum
;
SCAW=1000;		SCA Pulse Width (ns) [100|1000]
SCAI=1;			SCA Index: [#] (1-16)
SCAL=239;		SCA Low Threshold (0 to MCAC)
SCAH=252;		SCA High Threshold (0 to MCAC)
SCAO=HI;		SCA Active High/Low [OFF|HI|LO]
SCAI=2;			SCA Index: [#] (1-16)
SCAL=177;		SCA Low Threshold (0 to MCAC)
SCAH=199;		SCA High Threshold (0 to MCAC)
SCAO=HI;