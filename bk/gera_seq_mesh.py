#!/usr/local/bin/python3
#gera_tabela.py

import numpy as np
import os
import SYM_HexaPy
import epics
import sys
import time
from params import *

#a=True
#INPUT VARIABLES
#limits
x0=0
xf=0
nx=1

#PV'S
buttonpv = epics.PV('SYMETRIE:STATE#PANEL:BUTTON.VAL')
cmd_option=epics.PV('SYMETRIE:STATE#PANEL:SET.VAL')
cmd_status=epics.PV('SYMETRIE:STATE#PANEL:GET.VAL')
hexa_status=epics.PV('SYMETRIE:STATE#HEXA?')

#Position
y_pos = epics.PV('SYMETRIE:POSUSER:Y.VAL')
y_target=epics.PV('SYMETRIE:MOVE#PARAM:Y.VAL')
z_pos = epics.PV('SYMETRIE:POSUSER:Z.VAL')
z_target=epics.PV('SYMETRIE:MOVE#PARAM:Z.VAL')

#TABLE
table_run= epics.PV('SYMETRIE:SEQ#RUNNING.VAL')
table_index=epics.PV('SYMETRIE:SEQ#INDEX.VAL')




#___________________________________________________________________________________#
def scan_table():
#waiting for stabilization	
	cmd_option.put(value=12)
	while(cmd_option.value !=12):
		a=True		

#hold until table starts
	while(table_run.value ==0):
		a=True	
		#	print("waiting...to start table")
#hold until table ends
	cmd_option.put(value=0)
	while(table_run.value !=0):
		a=True	
		#print("waiting...to end table")
#___________________________________________________________________________________#
def uptable(t_stab,t_mes):
	t_mes=float(t_mes)
	t_stab=float(t_stab)
	#Parameters
	ip = "10.2.97.31"
	port = 1025
	SEQ_file_path = "/home/ABTLUS/sxs/hexapython/input_files/sequencia.txt"
	SEQ_pause_stab = t_stab
	#pause_m= float(input("tempo de medida="))
	SEQ_pause_mes =(t_mes)/2.0
	SEQ_dec_nb = 1


	#Connect to the controller
	hexapod = SYM_HexaPy.api()
	hexapod.connect(ip,port)
	hexapod.term("&2 Q20=2",False) #STOP command
	hexapod.SEQ_download(SEQ_file_path, SEQ_pause_stab, SEQ_pause_mes, SEQ_dec_nb)

	#hexapod.term("&2 Q20=12",False) #START sequence
	hexapod.disconnect()
#___________________________________________________________________________________#
def geratable(y0_,yf_,z0_,zf_,ny_,nz_,tstab_,tmes_):
	#print("Insira o valor iniciais de x0, y0_ e z0_")
	x0=0
	xf=0
	nx=1		
	print("Portanto os steps terão tamanho em mm de")
	print((xf-x0)/float(nx))
	print((yf_-y0_)/float(ny_))
	print((zf_-z0_)/float(nz_))
	hx=(xf-x0)/float(nx)
	hy=(yf_-y0_)/float(ny_)
	hz=(zf_-z0_)/float(nz_)
	print("Gerando o arquivo")
	print("*        *  ")
	print("*        *  ")
	#ANALYSING THE INPUTS
	ex=1
	ey=1
	ez=1
	if x0==xf:
	    ex=0

	if y0_==yf_:
	    ey=0

	if z0_==zf_:
	    ez=0
	params_path = "/home/ABTLUS/sxs/hexapython/params.py"
	with open(params_path, 'wb') as datafile_id:
		datafile_id.write(("y0=" + str(y0_)+ "\n").encode("utf8"))
		datafile_id.write(('yf='+str(yf_)+'\n').encode("utf8"))
		datafile_id.write(('ny='+str(ny_)+'\n').encode("utf8"))
		datafile_id.write(('z0='+str(z0_)+'\n').encode("utf8")) 
		datafile_id.write(('zf='+str(zf_)+'\n').encode("utf8")) 
		datafile_id.write(('nz='+str(nz_)+'\n').encode("utf8")) 
		datafile_id.write(('tstab='+str(tstab_)+'\n').encode("utf8")) 
		datafile_id.write(('tmes='+str(tmes_)+'\n').encode("utf8")) 
		datafile_id.write(('hy='+str(hy)+'\n').encode("utf8")) 
		datafile_id.write(('hz='+str(hz)+'\n').encode("utf8"))
	#LOOP TO FILL 
	datafile_path = "/home/ABTLUS/sxs/hexapython/input_files/sequencia.txt"
	opcao_de_mesh=input("Qual opcao de mesh?\n1 - ZIG-ZAG \n2 - Tabela\n3 - Step by Step\n")
	if opcao_de_mesh=="1":
		#datafile_path = "sequencia-teste1.txt"
		with open(datafile_path, 'w+') as f:
			m=5.2
			k=0
			while k<(int(nz_)+1):
				for j in range(0,((int(ny_)+2))):
					if int(k)%2==0:
						if j==(int(ny_)+1):
							f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k+1)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
							f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k+1)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
						else:
							f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
							f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
					else:
						if j==(int(ny_)+1):
							f.write((str(m)+'\t'+str(round((float((float(ny_)-j))*float(hy)-float(yf_)),6))+'\t'+str(round((float(z0_)+float(k+1)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
						else:
							f.write((str(m)+'\t'+str(round((float((float(ny_)-j))*float(hy)-float(yf_)),6))+'\t'+str(round((float(z0_)+float(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
							f.write((str(m)+'\t'+str(round((float((float(ny_)-j))*float(hy)-float(yf_)),6))+'\t'+str(round((float(z0_)+float(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))		
				k+=1
		datafile_path = "/home/ABTLUS/sxs/hexapython/input_files/sequencia_sem.txt"	
	
		with open(datafile_path, 'w+') as f:
			m=5.2
			k=0
			while k<(int(nz_)+1):
				for j in range(0,((int(ny_)+2))):
					if int(k)%2==0:
						f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
					else:
						f.write((str(m)+'\t'+str(round((float((float(ny_)-j))*float(hy)-float(yf_)),6))+'\t'+str(round((float(z0_)+float(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))	
				k+=1
	
		#For z axis

		datafile_path = "/home/ABTLUS/sxs/hexapython/input_files/eixoz"
		eixoz = np.zeros((int(nz_)+1)) 
		m=0
		for k in range(0,int(nz_)+1):
			eixoz[k]=z0_+(k)*hz
		dataz=eixoz
		with open(datafile_path, 'wb') as datafile_id:
			np.savetxt(datafile_id, dataz, fmt=['%.4f'])
		print("O arquivo gerado é")
		print("*        *  ")
		print("*        *  ")
		print("________________________________________________________________________________________")
		print('  x \t \t   y \t \t   z \t \t   Rx \t \t   Ry \t \t   Rz')
		print("________________________________________________________________________________________")
		os.system("cat /home/ABTLUS/sxs/hexapython/input_files/sequencia.txt")
		print("*        *  ")
		print("*        *  ")
		print("Z axis:")
		os.system("cat /home/ABTLUS/sxs/hexapython/input_files/eixoz")
		print("*        *  ")
		print("*        *  ")
		#*******************
		start=input("Start scan?  (yes/no)")
		if start=="yes":
			print("tempo",tstab_,tmes_)
			uptable(tstab_,tmes_)
		elif start=="no":
			print("tempo",tstab_,tmes_)
			uptable(tstab_,tmes_)	
			sys.exit()
		else:
			print("wrong option")
			sys.exit()	
		#*************

	elif opcao_de_mesh=="2":
		with open(datafile_path, 'w+') as f:
			m=5.2
			k=0
			while k<(int(nz_)+1):
				for j in range(0,((int(ny_)+2))):
					if j==(int(ny_)+1):
						f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
						f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
					else:
						f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
						f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))		
				k+=1
		datafile_path = "/home/ABTLUS/sxs/hexapython/input_files/sequencia_sem.txt"	
	
		with open(datafile_path, 'w+') as f:
			m=5.2
			k=0
			while k<(int(nz_)+1):
				for j in range(0,((int(ny_)+1))):
					f.write((str(m)+'\t'+str(round((float(j)*float(hy)+float(y0_)),6))+'\t'+str(round((float(z0_)+(k)*float(hz)),6))+'\t0\t0\t-15.0\n'))#.encode("utf8"))
				k+=1
	
		#For z axis

		datafile_path = "/home/ABTLUS/sxs/hexapython/input_files/eixoz"
		eixoz = np.zeros((int(nz_)+1)) 
		m=0
		for k in range(0,int(nz_)+1):
			eixoz[k]=z0_+(k)*hz
		dataz=eixoz
		with open(datafile_path, 'wb') as datafile_id:
			np.savetxt(datafile_id, dataz, fmt=['%.4f'])
		print("O arquivo gerado é")
		print("*        *  ")
		print("*        *  ")
		print("________________________________________________________________________________________")
		print('  x \t \t   y \t \t   z \t \t   Rx \t \t   Ry \t \t   Rz')
		print("________________________________________________________________________________________")
		os.system("cat /home/ABTLUS/sxs/hexapython/input_files/sequencia.txt")
		print("*        *  ")
		print("*        *  ")
		print("Z axis:")
		os.system("cat /home/ABTLUS/sxs/hexapython/input_files/eixoz")
		print("*        *  ")
		print("*        *  ")
		#*********
		start=input("Start scan?  (yes/no)")
		if start=="yes":
			print("tempo",tstab_,tmes_)
			uptable(tstab_,tmes_)
		elif start=="no":
			print("tempo",tstab_,tmes_)
			uptable(tstab_,tmes_)	
			sys.exit()
		else:
			print("wrong option")
			sys.exit()	
		#***********
	elif opcao_de_mesh=="3":
		start=input("Start scan?  (yes/no)")
		if start=="yes":
			#novafuncao
		elif start=="no":
			#novafuncao	
			sys.exit()
		else:
			print("wrong option")
			sys.exit()	
#___________________________________________________________________________________#
def new_mesh(_option):
	global y0
	global yf
	global z0
	global zf
	global ny
	global nz
	global tstab
	global tmes
	if _option=="no":
		import params
		geratable(params.y0,params.yf,params.z0,params.zf,params.ny,params.nz,params.tstab,params.tmes)
		y0=params.y0
		yf=params.yf
		z0=params.z0
		zf=params.zf
		ny=params.ny
		nz=params.nz
		tstab=params.tstab
		tmes=params.tmes
	elif _option=="yes":
	#INPUT VARIABLES
		y0=float(input("y0="))
		z0=float(input("z0="))

		print("Insira o valores finais de xf, yf_ e zf_")
		yf=float(input("yf="))
		zf=float(input("zf="))

		print("Portanto o range será (mm)")
		print('Em x de',x0, 'a', xf, 'mm')
		print('Em y de',y0, 'a', yf, 'mm')
		print('Em x de',z0, 'a', zf, 'mm')
		print("Quanto de passo para x, y e z?)")
		ny=input("n passos em y=")
		nz=input("n passos em z=")
		tstab=input("tstab_=")
		tmes=input("tmes_=")
		geratable(y0,yf,z0,zf,ny,nz,tstab,tmes)
		#uptable(tstab_,tmes_)
	else:
		print("wrong option")
		sys.exit()
############################
############################'
############################
############################
print("the actual parameters are:")
os.system("cat /home/ABTLUS/sxs/hexapython/params.py")
option=input("Do you want to change it?(yes/no)")
new_mesh(option)

'''start=input("Start scan?  (yes/no)")
if start=="yes":
	print("tempo",tstab,tmes)
	uptable(tstab,tmes)
elif start=="no":
	print("tempo",tstab,tmes)
	uptable(tstab,tmes)	
	sys.exit()
else:
	print("wrong option")
	sys.exit()	
'''

buttonpv.put(value=0)#STOP
inicio = time.time()
scan_table()#INITIATES SCAN
fim = time.time() - inicio
print("tempo:    ",fim)

