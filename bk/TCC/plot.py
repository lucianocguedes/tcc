import numpy as np
import os
import matplotlib.pyplot as plt
fname="TABELA"
a = np.loadtxt(fname,usecols=range(13))
vambora=a[:,0:2]
douglas=a[:,2:4]
classe=a[:,4:6]
aero=a[:,6:8]
aero30=a[:,8:10]
aero45=a[:,10:12]
values=a[:,12]
fig = plt.figure()
ax = fig.add_subplot(111,  xlim=(0,110), ylim=(0,5))
lines = {'linestyle': 'None'}
plt.rc('lines', **lines)
fmt1 = { 'color': '#13cec7', 'linestyle' : '-','marker':'.'}
fmt2 = { 'color': '#1499ce', 'linestyle' : '-','marker':'.'}
fmt3 = { 'color': '#144cce', 'linestyle' : '-','marker':'.'}
fmt4 = { 'color': '#5f14ce', 'linestyle' : '-.','marker':'.'}
fmt5 = { 'color': '#5f14ce', 'linestyle' : '-','marker':'.'}
fmt6 = { 'color': '#5f14ce', 'linestyle' : ':','marker':'.'}
p1=ax.errorbar(values,vambora[:,0], vambora[:,1],**fmt1,ecolor='#066360',capsize=3, elinewidth=1,linewidth=3)
p2=ax.errorbar(values,douglas[:,0], douglas[:,1],**fmt2,ecolor='#064d63',xuplims=True,capsize=3, elinewidth=1,linewidth=3)
p3=ax.errorbar(values,classe[:,0], classe[:,1],**fmt3,capsize=3, elinewidth=1,linewidth=3)
p4=ax.errorbar(values,aero[:,0], aero[:,1],**fmt4,capsize=3, elinewidth=1,linewidth=3)
p5=ax.errorbar(values,aero30[:,0], aero30[:,1],**fmt5,ecolor='k',capsize=3, elinewidth=1,linewidth=3)
p6=ax.errorbar(values,aero45[:,0], aero45[:,1],**fmt6,ecolor='k',capsize=3, elinewidth=1,linewidth=3)
#ax.set_xscale('log')
major_ticks = np.arange(0, 101, 10)
major_ticks_y = np.arange(0, 5, 1)
minor_ticks_y = np.arange(0, 5, 0.5)
minor_ticks = np.arange(0, 101, 5)

ax.set_xticks(major_ticks)
ax.tick_params(axis='both', which='major', labelsize=15)
ax.set_xticks(minor_ticks, minor=True)
ax.set_yticks(major_ticks_y)
ax.set_yticks(minor_ticks_y, minor=True)
plt.grid(True,which='major',ls=":")
plt.show()
